
package net.sasf.controller;

import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sasf.dto.AuthUser;
import net.sasf.dto.JWTResponse;
import net.sasf.service.security.JWTService;
import net.sasf.utils.model.ErrorDetalles;

@Path("/token")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthController {
    
    @Inject
    private JWTService jwtService;

    @POST
    public Response generateToken(AuthUser authUser) {

        if( authUser.getUsername().equals("admin")
            && authUser.getPassword().equals("admin")
            && authUser.getGrant_type().equals("client_credentials")) {

            JWTResponse response = new JWTResponse();
            response.setToken(jwtService.generateJwt());
            
            return Response.ok(response).build();
            
        }
        
        return Response.ok( 
            new ErrorDetalles(
                new Date(), 
                "Credenciales erroneas",
                "El tipo de autenticación o las credenciales otorgadas son incorrectas", 200
        )).build();        

    }

}
