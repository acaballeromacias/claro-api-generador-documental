
package net.sasf.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sasf.dto.DocumentResponse;
import net.sasf.dto.MergedPdfResponse;
import net.sasf.model.DatosPDA;
import net.sasf.service.EventLogService;
import net.sasf.service.FileService;
import net.sasf.service.contract.ContractService;
import net.sasf.service.imagen.ImagenService;

@RolesAllowed("writer")
@Path("/generarPdf")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ContractController {

    @Inject
    private ContractService documentGeneratorService;
    @Inject
    private FileService fileService;
    @Inject
    private ImagenService imagenService;
    @Inject
    private EventLogService eventLogService;

    @POST
    public Response generateContract(@Valid DatosPDA datosPda) {
        try {

            eventLogService.saveLog(datosPda.getRequestId(), "Inicio de peticion",
                    "Se llamó al endpoint /generarPdf",
                    "", "I");   

            if (!(datosPda.getOrderId() == null || datosPda.getOrderId().isEmpty())) {

                eventLogService.saveLog(datosPda.getRequestId(), "Inicio del proceso de guardado de la imagen",
                        "Se inició el proceso para guardar una nueva imagen en el servidor",
                        "", "I");

                if (datosPda.getImgCedulaCliente1() == null || datosPda.getImgCedulaCliente1().isEmpty()) {

                    throw new NoResultException("No han llegado correctamente las imagenes");

                } else if (imagenService.convertAndSendImage(datosPda.getImgCedulaCliente1()) == 1) {

                    eventLogService.saveLog(datosPda.getRequestId(), "Guardado de imagen exitoso",
                            "Se guardó exitosamente la imagen en el servidor",
                            "", "F");

                    return Response.ok("{\"msg\":\"Imagen correctamente generada\"}").build();

                } else {

                    throw new NoResultException("No han llegado correctamente las imagenes");

                }

            } else {

                eventLogService.saveLog(datosPda.getRequestId(), "Generación de documento",
                        "Se inició el proceso para la generación del documento: SIGNED_"
                                + datosPda.getIdTransactionPDA() + "_v1.pdf",
                        "", "I");

                MergedPdfResponse pdfmerged = this.documentGeneratorService.generateContract(datosPda);
                DocumentResponse document = new DocumentResponse("SIGNED_" + datosPda.getIdTransactionPDA() + "_v1.pdf",
                        "PDF", pdfmerged.getMergedPdf());

                List<DocumentResponse> documents = new ArrayList<>();
                documents.add(document);

                List<File> documentFiles = fileService.convertDocumentResponsesToFiles(documents);

                eventLogService.saveLog(datosPda.getRequestId(), "Documento generado",
                        "Se generó con exito el documento SIGNED_" + datosPda.getIdTransactionPDA() + "_v1.pdf",
                        "", "N");

                return fileService.uploadFiles(documentFiles, datosPda.getRequestId());

                // return Response.ok(document).build();
            }

        } catch (Exception e) {

            throw new NoResultException(e.getMessage());

        }

    }

}
