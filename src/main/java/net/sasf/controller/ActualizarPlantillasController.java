
package net.sasf.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
// import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.quarkus.panache.common.Sort;
import net.sasf.model.CSPlantilla;
import net.sasf.service.EventLogService;
import net.sasf.service.contract.ActualizarPlantillaService;

@Path("/actualizarPlantillas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("writer")
public class ActualizarPlantillasController {

    @Inject
    private ActualizarPlantillaService actualizarPlantillaService;

    @Inject
    private EventLogService eventLogService;

    @GET
    public Response getTemplate() {

        eventLogService.saveLog("", "Inicio de petición.",
                    "Se llamó al endpoint /actualizarPlantillas", "", "I");

        List<Long> idTemplates = List.of(
                941l, 970l, 795l, 24l, 956l, 233l, 946l, 15l,
                952l, 945l, 991l, 989l, 963l, 985l, 981l, 914l, 930l, 986l,
                988l, 1030l, 1040l, 1105l, 1106l, 1154l, 1167l);

        List<CSPlantilla> plantillas = CSPlantilla.list("idPlantilla in ?1", Sort.by("idPlantilla"), idTemplates);
        try {
            for (CSPlantilla plantilla : plantillas) {
                actualizarPlantillaService.editarPlantilla(plantilla);
            }
        } catch (Exception e) {

            eventLogService.saveLog("", "Error al actualizar plantillas.",
                    e.getMessage(), "", "E");

            throw new RuntimeException(e.getMessage());
        }

        eventLogService.saveLog("", "Plantillas actualizadas.",
                    "El proceso de actualizar plantillas se ejecutó sin problemas.", "", "N");

        return Response.status(Response.Status.CREATED)
                .entity("{\"message\": \"Plantillas actualizadas exitosamente\"}")
                .build();
    }

}
