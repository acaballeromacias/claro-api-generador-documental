package net.sasf.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import com.oracle.svm.core.annotate.Inject;

import net.sasf.dto.DocumentResponse;
import net.sasf.utils.ConfigLoader;

@ApplicationScoped
public class FileService {

    @Inject
    private AuthService _authService;

    @Inject
    private EventLogService _eventLogService;

    public FileService(AuthService authService, EventLogService eventLogService) {
        this._authService = authService;
        this._eventLogService = eventLogService;
    }

    public Response uploadFiles(List<File> files, String requestId) {

        _eventLogService.saveLog(requestId, "Inicio del proceso de envio a firma electrónica",
                "Se envió exitosamente a firmar el archivo pdf.",
                "", "I");

        try {
            String token = _authService.getToken();
            try (CloseableHttpClient httpClient = createHttpClient()) {
                HttpPost httpPost = new HttpPost(
                        ConfigLoader.GetSettings().getBASE_URL() + ConfigLoader.GetSettings().getUPLOAD_DOCUMENTS());
                HttpEntity entity = createMultipartEntity(files, requestId);

                httpPost.setHeader("Authorization", "Bearer " + token);
                httpPost.setEntity(entity);

                HttpResponse response = httpClient.execute(httpPost);

                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                String responseBody = EntityUtils.toString(response.getEntity(), "UTF-8");

                if (statusCode == 200) {
                    return Response.ok(responseBody).build();
                } else {
                    return Response.status(statusCode).entity(responseBody).build();
                }
            }
        } catch (IOException e) {
            return Response.serverError().entity("Error al cargar los archivos.").build();
        }
    }

    private HttpEntity createMultipartEntity(List<File> files, String requestId) {
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();

        entityBuilder.addTextBody("requestId", requestId);

        for (File file : files) {
            entityBuilder.addBinaryBody("files", file, ContentType.DEFAULT_BINARY, file.getName());
        }

        return entityBuilder.build();
    }

    private CloseableHttpClient createHttpClient() {
        try {
            if (ConfigLoader.GetSettings().getUSE_SSL()) {
                return HttpClients.createDefault();
            } else {
                return HttpClients.custom()
                        .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                        .setSSLContext(new SSLContextBuilder()
                                .loadTrustMaterial(null, new TrustStrategy() {
                                    public boolean isTrusted(X509Certificate[] arg0, String arg1)
                                            throws CertificateException {
                                        return true;
                                    }
                                })
                                .build())
                        .build();
            }
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<File> convertDocumentResponsesToFiles(List<DocumentResponse> documentResponses) {
        List<File> files = new ArrayList<>();

        for (DocumentResponse documentResponse : documentResponses) {
            byte[] data = documentResponse.getDocument();
            File tempFile = createTempFileFromByteArray(data);
            files.add(tempFile);
        }

        return files;
    }

    private File createTempFileFromByteArray(byte[] data) {
        try {
            File tempFile = File.createTempFile("document", ".pdf");
            FileOutputStream fos = new FileOutputStream(tempFile);
            fos.write(data);
            fos.close();
            return tempFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
