package net.sasf.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import java.time.LocalDateTime;

import net.sasf.model.EventLog;
import net.sasf.repository.EventLogRepository;

@ApplicationScoped
public class EventLogService {

    @Inject
    EventLogRepository eventLogRepository;

    @Transactional
    public void saveLog(String requestId, String descripcion, String detalle, String ruta, String tipo) {
        EventLog logEntry = new EventLog();
        logEntry.setRequestId(requestId);
        logEntry.setDescripcion(descripcion);
        logEntry.setDetalle(detalle);
        logEntry.setRuta(ruta);
        logEntry.setFechaCreacion(LocalDateTime.now());
        logEntry.setTipo(tipo);

        eventLogRepository.persist(logEntry);
    }
}
