
package net.sasf.service.security;

import io.smallrye.jwt.build.Jwt;
import javax.inject.Singleton;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@Singleton
public class JWTService {

    @ConfigProperty(name = "jwt.expiration")
	private Integer time;
    
    @ConfigProperty(name = "jwt.issuer")
	private String issuer;

    public String generateJwt() {
        return Jwt.issuer(this.issuer)
            .subject("")
            .groups("writer")
            .expiresAt(System.currentTimeMillis() + time)
            .sign();
        
    }

}
