package net.sasf.service.imagen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TransactionRequiredException;
import net.sasf.utils.ConfigHandler;

@ApplicationScoped
public class ImagenService {

    public int convertAndSendImage(String image) throws IOException {
        try {

            String imagesFolderPath = String.format("/%s/", ConfigHandler.IMAGES_PATH);

            File imagesFolder = new File(imagesFolderPath);
            if (!imagesFolder.exists()) {
                if (!imagesFolder.mkdirs()) {
                    throw new TransactionRequiredException("Error al crear la carpeta 'templates'.");
                }
            }

            byte[] imageBytes = Base64.getDecoder().decode(image);

            String imagesPath = String.format("%s/%s", imagesFolder, UUID.randomUUID() +
                    ".jpg");

            FileOutputStream fileOutputStream = new FileOutputStream(imagesPath);
            fileOutputStream.write(imageBytes);
            fileOutputStream.close();

            return 1;

        } catch (Exception e) {
            // Handle exception
            e.printStackTrace();
            return 0;
        }
    }
}
