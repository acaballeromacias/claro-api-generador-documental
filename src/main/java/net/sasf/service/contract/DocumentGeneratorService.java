
package net.sasf.service.contract;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.TransactionRequiredException;

import org.xhtmlrenderer.pdf.ITextRenderer;

import io.quarkus.qute.Engine;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import net.sasf.service.EventLogService;
import net.sasf.utils.ConfigHandler;
import net.sasf.utils.TemplateContext;

@ApplicationScoped
public class DocumentGeneratorService {

    @Inject
    private Engine templateEngine;

    @Inject
    private EventLogService eventLogService;

    public byte[] generateFile(String templateName, TemplateContext<?> context, String requestId) {

        try {

            TemplateInstance templateInstance = getTemplateInstance(templateName, context, requestId);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();

            renderer.setDocumentFromString(templateInstance.render());
            renderer.layout();
            // renderer.getFontResolver().getFontFamily(templateName);
            renderer.createPDF(outputStream, true);

            return outputStream.toByteArray();

        } catch (Exception e) {

            eventLogService.saveLog(requestId, "Error de generación de documento.",
                    "Error al generar el documento " + templateName + " " + e.getMessage(),
                    "", "E");

            e.printStackTrace();
            throw new TransactionRequiredException(
                    "Error al generar el documento " + templateName + " " + e.getMessage());

        }

    }

    private TemplateInstance getTemplateInstance(
            String templateName, TemplateContext<?> context, String requestId) throws Exception {

        try {

            Path templatePath = Paths.get(
                    String.format("/%s/%s.html",
                            ConfigHandler.TEMPLATE_PATH,
                            templateName));

            Template template = this.templateEngine.parse(
                    Files.readString(templatePath.toAbsolutePath(), StandardCharsets.UTF_8));
            return template.data("data", context.getData());

        } catch (Exception e) {

            eventLogService.saveLog(requestId, "Error en la lectura de la plantilla.",
                    "Error al obtener la plantilla " + templateName,
                    "", "E");

            e.printStackTrace();
            throw new Exception("Error al obtener la plantilla " + templateName);
        }

    }

}
