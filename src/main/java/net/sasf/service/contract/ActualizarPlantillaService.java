package net.sasf.service.contract;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.sasf.model.CSPlantilla;
import net.sasf.model.MetadatosPDA;
import net.sasf.service.EventLogService;
import net.sasf.utils.ConfigHandler;

@ApplicationScoped
public class ActualizarPlantillaService {

    @Inject
    private EventLogService eventLogService;

    public void editarPlantilla(CSPlantilla plantilla) {
        Long idPlantilla = plantilla.getIdPlantilla();
        String contenido = plantilla.getContenido();

        String contenidoActualizado = "";

        switch (idPlantilla.intValue()) {

            case 15:
                contenidoActualizado = editaDoc15(contenido);
                break;
            case 24:
                contenidoActualizado = editaDoc24(contenido);
                break;
            case 233:
                contenidoActualizado = editaDoc233(contenido);
                break;
            case 795:
                contenidoActualizado = editaDoc795(contenido);
                break;
            case 914:
                contenidoActualizado = editaDoc914(contenido);
                break;
            case 930:
                contenidoActualizado = editaDoc930(contenido);
                break;
            case 941:
                contenidoActualizado = editaDoc941(contenido);
                break;
            case 945:
                contenidoActualizado = editaDoc945(contenido);
                break;
            case 946:
                contenidoActualizado = editaDoc946(contenido);
                break;
            case 952:
                contenidoActualizado = editaDoc952(contenido);
                break;
            case 956:
                contenidoActualizado = editaDoc956(contenido);
                break;
            case 970:
                contenidoActualizado = editaDoc970(contenido);
                break;
            case 981:
                contenidoActualizado = editaDoc981(contenido);
                break;
            case 985:
                contenidoActualizado = editaDoc985(contenido);
                break;
            case 988:
                contenidoActualizado = editaDoc988(contenido);
                break;
            case 989:
                contenidoActualizado = editaDoc989(contenido);
                break;
            case 991:
                contenidoActualizado = editaDoc991(contenido);
                break;
            case 1030:
                contenidoActualizado = editaDoc1030(contenido);
                break;
            case 1040:
                contenidoActualizado = editaDoc1040(contenido);
                break;
            case 1105:
                contenidoActualizado = editaDoc1105(contenido);
                break;
            case 1106:
                contenidoActualizado = editaDoc1106(contenido);
                break;
            case 1154:
                contenidoActualizado = editaDoc991(contenido);
                break;
            case 1167:
                contenidoActualizado = editaDoc1167(contenido);
                break;

            default:
                // Lógica para manejar IDs desconocidos
                break;
        }

        String plantillaHtml = completarHtml(contenidoActualizado);
        generarArchivoHtml(idPlantilla, plantillaHtml);

    }

    private void generarArchivoHtml(Long idPlantilla, String contenidoHtml) {
        try {
            String templateFolderPath = String.format("/%s/", ConfigHandler.TEMPLATE_PATH);

            File templateFolder = new File(templateFolderPath);
            if (!templateFolder.exists()) {
                if (!templateFolder.mkdirs()) {

                    eventLogService.saveLog("", "Error al actualizar plantillas.",
                            "Error al crear la carpeta 'templates'.", "", "E");

                    throw new RuntimeException("Error al crear la carpeta 'templates'.");
                }
            }

            File archivo = new File(
                    Paths.get(templateFolderPath, idPlantilla.toString() + ".html")
                            .toAbsolutePath()
                            .toString());

            FileWriter writer = new FileWriter(archivo, false);
            writer.write(contenidoHtml);
            writer.close();
        } catch (IOException e) {

            eventLogService.saveLog("", "Error al actualizar plantillas.",
                    "Error al generar el archivo HTML " + e.getMessage(), "", "E");

            e.printStackTrace();
            throw new RuntimeException("Error al generar el archivo HTML " + e.getMessage());
        }
    }

    // SIN USO - CARTA DE AUTORIZACIÓN MEDIOS ELECTRÓNICOS
    private String editaDoc795(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 795" + e.getMessage());
        }
        return result;
    }

    // ======================aceptacion_promocion_ott=989======================
    private String editaDoc989(String plantilla) {
        String result = "";

        try {

            result = plantilla
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_TELEFONIA_FIJO, "{data.tieneTelefoniaFijo}")
                    .replaceAll(MetadatosPDA.MD_INTERNET_FIJO, "{data.tieneInternetFijo}")
                    .replaceAll(MetadatosPDA.MD_OTT_FIJO, "{data.tieneOttFijo}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_INT, "{data.afterSuscInternet}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_INT, "{data.beforeSuscInternet}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_TEL, "{data.afterSuscTelefonia}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_TEL, "{data.beforeSuscTelefonia}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_INSTALACION, "{data.totalInstalacionOtt}")
                    .replace(MetadatosPDA.MD_BEFORE_SUSC_OTT, "{data.beforeSuscOtt}")
                    .replace(MetadatosPDA.MD_AFTER_SUSC_OTT, "{data.afterSuscOtt}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 989" + e.getMessage());
        }
        return result;
    }

    // ======================aceptacion_promocion=991======================
    private String editaDoc991(String plantilla) {
        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_FECHA_ACTUAL, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_GENERAL_INSTAL, "0.00")
                    .replaceAll(MetadatosPDA.MD_GENERAL_DESC, "0.00")
                    .replaceAll(MetadatosPDA.MD_TELEVISION_FIJO, "{data.tieneTvFijo}")
                    .replaceAll(MetadatosPDA.MD_TELEFONIA_FIJO, "{data.tieneTelefoniaFijo}")
                    .replaceAll(MetadatosPDA.MD_INTERNET_FIJO, "{data.tieneInternetFijo}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_INT, "{data.afterSuscInternet}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_INT, "{data.beforeSuscInternet}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_TEL, "{data.beforeSuscTelefonia}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_TEL, "{data.afterSuscTelefonia}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_TV, "{data.beforeSuscTv}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_TV, "{data.afterSuscTv}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_INSTALACION, "{data.totalInstalacion}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 991" + e.getMessage());
        }
        return result;
    }

    // ======================financimiento=233======================
    private String editaDoc233(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_USUARIO_DIGITADOR, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_FECHA_ACTUAL, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_USUARIO, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_ID_OFIC, "{data.datosRequest.idOficina}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_NOMBRE, "{data.datosRequest.oficinaNombre}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES,
                            "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_SOLICITUD_ID, "{data.datosRequest.financiamiento}")
                    .replaceAll(MetadatosPDA.MD_NUM_CUENTA_BSCS, "{data.datosRequest.numeroCuentaBSCS}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_SERVICIO, "{data.datosRequest.clienteServicio}")
                    .replaceAll(MetadatosPDA.MD_BASE_FINANCIAMIENTO, "{data.datosRequest.valorBaseFinanciamiento}")
                    .replaceAll(MetadatosPDA.MD_VALOR_IVA_FINANCIAMIENTO, "{data.datosRequest.valorIvaFinanciamiento}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_CUOTA_INICIAL, "{data.datosRequest.cuotaInicial}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_TIPOIDEN, "{data.datosRequest.clienteTipoIdentificacion}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_USUARIO, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_SUMA_VALORES, "{data.sumaValores1Formateada}")
                    .replaceAll(MetadatosPDA.MD_TABLA_DET_FIN_SGA, "{data.tablaDetalleFinanciamiento}")
                    .replaceAll(MetadatosPDA.MD_TABLA_CUOTAS_FIN, "{data.tablaCoutaFinanciamiento}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "{data.datosRequest.idTransactionPDA}")
            // .replaceAll(MetadatosPDA.MD_ID_FINAN , "%%FINANCIAMIENTO%%")
            ; // No esta

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 233" + e.getMessage());
        }
        return result;
    }

    // ======================anexo_tv=970======================
    private String editaDoc970(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_PLAN_ACT_TV, "{data.datosRequest.plantv}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_SOLICITUD_ID, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_BENEF_CLI_TV, "{data.perminBenefCliTv}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_CLI_TV, "{data.radioButtonPerminCli}")
                    .replaceAll(MetadatosPDA.MD_PAGO_FORMA_FIJO, "{data.radioButtonPagoFijo}")
                    .replaceAll(MetadatosPDA.MD_CODIGO_VENDEDOR, "{data.datosRequest.codigoVendedor}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_DOM_PROVINCIA1, "{data.datosRequest.domProvinciaInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_CANTON, "{data.datosRequest.domCantonInst}")
                    .replaceAll(MetadatosPDA.MD_DIRRECCION_INSTAL, "{data.datosRequest.direccionInstalacion}")
                    .replaceAll(MetadatosPDA.MD_PRICE_BEFORE_TV, "{data.datosRequest.priceBeforeTv}")
                    .replaceAll(MetadatosPDA.MD_PRICE_AFTER_TV, "{data.datosRequest.priceAfterTv}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_TV, "{data.datosRequest.afterSuscTv}")
                    .replaceAll(MetadatosPDA.MD_ADICIONAL_TELEVISION, "{data.adicionalTelevisor}")
                    .replaceAll(MetadatosPDA.MD_PAGO_UNICO_FIJO, "{data.tablaServiciosPagoUnicoTel}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_TIEMPO_CLI_TV, "{data.datosRequest.perminTiempoCli}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_TV, "{data.datosRequest.totalTvImp}")
                    .replaceAll(MetadatosPDA.MD_CANALES_AUDIO_TV, "{data.datosRequest.canalesAudioTv}")
                    .replaceAll(MetadatosPDA.MD_CANALES_VIDEO_TV, "{data.datosRequest.canalesVideoTv}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NOMBRE_COMPLETO, "{data.datosRequest.bancoNombreCompleto}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NUMERO_CUENTA_BANCARIA, "{data.datosRequest.bancoNumeroCuenta}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NUMERO, "{data.datosRequest.tarjetaNumero}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NOMBRE_CLIENTE, "{data.datosRequest.nombreTitularTarjeta}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_TITULAR_BAN, "{data.datosRequest.nombreTitularCta}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_TITULAR_BAN, "{data.datosRequest.nombreTitularCta}");

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 970" + e.getMessage());
        }
        return result;
    }

    // ======================anexo_privacidad_clientes=1040======================
    private String editaDoc1040(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_CEDULA, "{data.datosRequest.clienteCedula}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 1040" + e.getMessage());
        }
        return result;
    }

    // ======================aceptacion_empaquetamiento=1030======================
    private String editaDoc1030(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll("e56ea345", " e56ea345")
                    .replaceAll(MetadatosPDA.MD_SOLICITUD_ID, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_PAQUETE_FMC, "{data.datosRequest.paqueteFmc}")
                    .replaceAll(MetadatosPDA.MD_TAMANIO_FMC, "{data.datosRequest.tamanioFmc}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CABECERA_FMC, "{data.strCab}")
                    .replaceAll(MetadatosPDA.MD_DETALLE_FMC, "{data.strDet}")
                    .replaceAll(MetadatosPDA.MD_PRICE_BEFORE_FMC, "{data.priceBeforeFmc}")
                    .replaceAll(MetadatosPDA.MD_PRICE_AFTER_FMC, "{data.priceAfterFmc}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 1030" + e.getMessage());
        }
        return result;
    }

    // ======================tirilla_contratacion_ofertas=986======================
    private String editaDoc1167(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_TIPOIDEN, "{data.datosRequest.clienteTipoIdentificacion}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_PERSONA_EMAIL, "{data.datosRequest.personaEmail}")
                    .replaceAll(MetadatosPDA.MD_FECHA_TRAMITE, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_USUARIO_DIGITADOR, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_COMENTARIO_ASESOR, "{data.datosRequest.ottTirilla.observacionAsesor}")
                    .replaceAll(MetadatosPDA.MD_PLAN_ACT_OTT,
                            "{data.datosRequest.ottTirilla.ofertaNueva}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_OTT, "{data.datosRequest.totalOtt}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_NOMBRE, "{data.datosRequest.oficinaNombre}")
                    .replaceAll(MetadatosPDA.MD_SERVICE_OTT, "{data.datosRequest.clienteServicio}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 1167" + e.getMessage());
        }
        return result;
    }

    // ======================renta_equipo=981======================
    private String editaDoc981(String plantilla) {
        String result = "";
        try {
            result = plantilla

                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.nombreCompletoBanco}")
                    .replaceAll(MetadatosPDA.MD_TABLA_DECO_PRINCIPAL, "{data.tablaDecoPrincipal}")
                    .replace(MetadatosPDA.MD_TABLA_DECO_ADICIOALES, "{data.tablaDecoAdicional}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 981" + e.getMessage());
        }
        return result;
    }

    // ======================ternimo_condiciones=985======================
    private String editaDoc985(String plantilla) {
        String result = "";
        try {

            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}");

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 985" + e.getMessage());
        }
        return result;
    }

    // ======================anexo_condiciones=945======================
    private String editaDoc945(String plantilla) {
        String result = "";
        try {

            result = plantilla;

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 945" + e.getMessage());
        }
        return result;
    }

    // ======================ofertas_suplementaria=952======================
    private String editaDoc952(String plantilla) {

        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_FECHA_TRAMITE, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_USUARIO_DIGITADOR, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_NOMBRE, "{data.datosRequest.oficinaNombre}")
                    .replaceAll(MetadatosPDA.MD_ID_SERVICIO, "{data.datosRequest.clienteServicio}")
                    .replaceAll(MetadatosPDA.MD_PERSONA_EMAIL, "{data.datosRequest.personaEmail}")
                    .replaceAll(MetadatosPDA.MD_OFERTAS_ACTIVAN,
                            "{data.ofertasRecursos.ofertaSuplementaria}")
                    .replaceAll(MetadatosPDA.MD_COMENTARIO_ASESOR, "{data.ofertasRecursos.observacionGeneral}")
                    .replaceAll(MetadatosPDA.MD_TIPO_SUSCRIP, "{data.ofertasRecursos.tipoSuscripcion}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_TIPOIDEN, "{data.datosRequest.clienteTipoIdentificacion}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_FUTURE_DATE, "{data.ofertasRecursos.fechaEjecucion}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 952" + e.getMessage());
        }
        return result;
    }

    // correo_notificacion=15
    private String editaDoc15(String plantilla) {

        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll("%%MAIL:GEO%%", "")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "{data.datosRequest.idTransactionPDA}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 15" + e.getMessage());
        }
        return result;
    }

    // pagare=946
    private String editaDoc946(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES,
                            "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_TIPOIDEN, "{data.datosRequest.clienteTipoIdentificacion}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NOMBRE_COMPLETO, "{data.datosRequest.bancoNombreCompleto}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_FORMA_PAGODEB, "{data.tipoTarjetatipoBanco}")
                    .replaceAll(MetadatosPDA.MD_BANCO_CUENTA_DEB, "{data.tipoBancoTipoTarjeta}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NUMERO, "{data.datosRequest.tarjetaNumero}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NUMERO_CUENTA_BANCARIA, "{data.datosRequest.bancoNumeroCuenta}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES, "{data.datosRequest.nombreTitularTarjeta}")
                    .replaceAll(MetadatosPDA.MD_SUMA_VALORES, "{data.sumaValores1Formateada}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_FECHA_ACTUAL, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_LETRAS, "{data.datosRequest.totalLetras}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_TABLA_PAGARE_FIJO, "{data.tablaPagareFijo}");

            ;
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 946" + e.getMessage());
        }
        return result;
    }

    // factura_electronica=956
    private String editaDoc956(String plantilla) {
        String result = "";
        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_TIPOIDEN, "{data.datosRequest.clienteTipoIdentificacion}")
                    .replaceAll(MetadatosPDA.MD_DOM_TELEF, "{data.datosRequest.domicilioTelefono}")
                    .replaceAll(MetadatosPDA.MD_NUM_CUENTA_BSCS, "{data.datosRequest.numeroCuentaBSCS}")
                    .replaceAll(MetadatosPDA.MD_CORREO_PERSONA, "{data.datosRequest.personaEmail}")
                    .replaceAll(MetadatosPDA.MD_ID_SERVICIO, "{data.datosRequest.clienteServicio}")
                    .replaceAll(MetadatosPDA.MD_NUMERO_CUENTA_FACTURACION,
                            "{data.datosRequest.numeroCuentaFacturacion}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 956" + e.getMessage());
        }
        return result;
    }

    // doc948-anexo_internet
    private String editaDoc1105(String plantilla) {
        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_SOLICITUD_ID, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_CODIGO_VENDEDOR, "{data.datosRequest.codigoVendedor}")
                    .replaceAll(MetadatosPDA.MD_DOM_CANTON, "{data.datosRequest.domCantonInst}")
                    .replaceAll(MetadatosPDA.MD_DIRRECCION_INSTAL, "{data.datosRequest.direccionInstalacion}")
                    .replaceAll(MetadatosPDA.MD_PLAN_ACT_INT, "{data.datosRequest.planActualInternet}")
                    .replaceAll(MetadatosPDA.MD_PRICE_BEFORE_INT, "{data.datosRequest.priceBeforeInternet}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_INSTAL_PARROQUIA, "{data.datosRequest.domParroqInst}")
                    .replaceAll(MetadatosPDA.MD_PRICE_AFTER_INT, "{data.afterBeforeInternet}")
                    .replaceAll(MetadatosPDA.MD_ADICIONAL_INTERNET, "{data.adicionalInternet}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_INT, "{data.totalInternet}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_INT, "{data.beforeSuscInternet}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_INT, "{data.afterSuscInternet}")
                    .replaceAll(MetadatosPDA.MD_VALOR_DEPOSITO_GARANTIA_INT,
                            "{data.datosRequest.valorDepositoGarantiaInt}")
                    .replaceAll(MetadatosPDA.MD_DEPOSITO_GARANTIA_INT, "{data.radioButtonDepositoGarantia}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_CLI_INT, "{data.radioButtonPerminCli}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_TIEMPO_CLI_INT, "{data.datosRequest.perminTiempoCli}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_BENEF_CLI_INT, "{data.perminBenefCli}")
                    .replaceAll(MetadatosPDA.MD_VELOCIDAD_MIN, "{data.datosRequest.velocidadMin}")
                    .replaceAll(MetadatosPDA.MD_VELOCIDAD_MAX, "{data.datosRequest.velocidadMax}")
                    .replaceAll(MetadatosPDA.MD_PAGO_FORMA_FIJO, "{data.radioButtonPagoFijo}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NOMBRE_COMPLETO, "{data.datosRequest.bancoNombreCompleto}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NUMERO_CUENTA_BANCARIA, "{data.datosRequest.bancoNumeroCuenta}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NUMERO, "{data.datosRequest.tarjetaNumero}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NOMBRE_CLIENTE, "{data.nombreBancarioCondicional}")
                    .replaceAll(MetadatosPDA.MD_DOM_PROVINCIA1, "{data.datosRequest.domProvinciaInst}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_TITULAR_BAN, "{data.datosRequest.nombreTitularCta}")
                    ;

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 1105" + e.getMessage());
        }
        return result;
    }

    // doc941-contrato
    private String editaDoc941(String plantilla) {
        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_PERSONA_EMAIL, "{data.datosRequest.personaEmail}")
                    .replaceAll(MetadatosPDA.MD_DIRRECCION_INSTAL, "{data.datosRequest.direccionInstalacion}")
                    .replaceAll(MetadatosPDA.MD_DOM_TELEF, "{data.datosRequest.domicilioTelefono}")
                    .replaceAll(MetadatosPDA.MD_DOM_PROVINCIA, "{data.datosRequest.domProvinciaInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_CIUDAD, "{data.datosRequest.domCiudadInst}")
                    .replaceAll(MetadatosPDA.MD_TRECERA_EDAD, "{data.radioButtonTerceraEdad}")
                    .replaceAll(MetadatosPDA.MD_CLIEN_DISCA, "{data.radioButtonClienteDiscap}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_DOM_DIR_COMP, "{data.datosRequest.domicilioDirCompleta}")
                    .replaceAll(MetadatosPDA.MD_DOM_PARROQUIA, "{data.datosRequest.domParroqInst}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 941" + e.getMessage());
        }
        return result;
    }

    // doc24-autorizacion_debito
    private String editaDoc24(String plantilla) {

        String result = "";

        try {

            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.identificacionBancaria}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_TIPOIDEN, "{data.datosRequest.clienteTipoIdentificacion}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRES,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}") //
                    .replaceAll(MetadatosPDA.MD_BANCO_NOMBRE_COMPLETO, "{data.datosRequest.bancoNombreCompleto}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_FORMA_PAGODEB, "{data.tipoTarjetatipoBanco}")
                    .replaceAll(MetadatosPDA.MD_BANCO_CUENTA_DEB, "{data.tipoBancoTipoTarjeta}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NUMERO, "{data.datosRequest.tarjetaNumero}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NUMERO_CUENTA_BANCARIA, "{data.datosRequest.bancoNumeroCuenta}");

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 24" + e.getMessage());

        }

        return result;

    }

    // doc949-anexo_telefonia
    private String editaDoc1106(String plantilla) {
        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_SERVICE_TEL, "{data.datosRequest.clienteServicio}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_DOM_PROVINCIA1, "{data.datosRequest.domProvinciaInst}")
                    .replaceAll(MetadatosPDA.MD_CODIGO_VENDEDOR, "{data.datosRequest.codigoVendedor}")
                    .replaceAll(MetadatosPDA.MD_SOLICITUD_ID, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_DOM_CANTON, "{data.datosRequest.domCantonInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_PARROQUIA, "{data.datosRequest.domParroqInst}")
                    .replaceAll(MetadatosPDA.MD_DIRRECCION_INSTAL, "{data.datosRequest.direccionInstalacion}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}") //
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_DOM_CIUDAD, "{data.datosRequest.domCiudadInst}")
                    .replaceAll(MetadatosPDA.MD_INSTAL_PARROQUIA, "{data.datosRequest.domParroqInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_CALLE_P, "{data.datosRequest.domCallePInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_CALLE_S, "{data.datosRequest.domCalleSInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_NUMERACION, "{data.datosRequest.domNumeracInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_EDIFICIO, "{data.datosRequest.domEdifInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_PISO, "{data.datosRequest.domPisoInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_OFICINA, "{data.datosRequest.domOficInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_REFERENCIA, "{data.datosRequest.domRefInst}")
                    .replaceAll(MetadatosPDA.MD_PLAN_ACT_TEL, "{data..datosRequest.planActualTelefonia}")
                    .replaceAll(MetadatosPDA.MD_OFF_NET, "{data.datosRequest.minutosOffnet}")
                    .replaceAll(MetadatosPDA.MD_ON_NET, "{data.datosRequest.minutosOnnet}")

                    .replaceAll(MetadatosPDA.MD_PRICE_BEFORE_TEL, "{data.datosRequest.priceBeforeTelefonia}")
                    .replaceAll(MetadatosPDA.MD_PRICE_AFTER_TEL, "{data.priceAfterTelefonia}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_TEL, "{data.beforeSuscTel}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_TEL, "{data.afterSuscTel}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_CLI_TEL, "{data.radioButtonPerminCli}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_TIEMPO_CLI_TEL, "{data.perminTiempoCli}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_BENEF_CLI_TEL, "{data.telefonoCli}") 
                    .replaceAll(MetadatosPDA.MD_TOTAL_TEL, "{data.totalTelImp}")
                    .replaceAll(MetadatosPDA.MD_PAGO_FORMA_FIJO, "{data.radioButtonPagoFijo}") 
                    .replaceAll(MetadatosPDA.MD_BANCO_NOMBRE_COMPLETO, "{data.datosRequest.bancoNombreCompleto}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NUMERO_CUENTA_BANCARIA, "{data.datosRequest.bancoNumeroCuenta}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NUMERO, "{data.datosRequest.tarjetaNumero}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NOMBRE_CLIENTE, "{data.datosRequest.nombreTitularTarjeta}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_TITULAR_BAN, "{data.datosRequest.nombreTitularCta}")
                    .replaceAll(MetadatosPDA.MD_ADICIONAL_TELEFONIA, "{data.adicionalTelefonia}");
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 1106" + e.getMessage());
        }
        return result;
    }

    // doc930-pagare_orden_hwi
    public String editaDoc930(String plantilla) {
        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "" + "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES,
                            "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "" + "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_SUMA_VALORES, "{data.sumaValores1Formateada}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_FECHA_ACTUAL, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_TABLA_PAGARE_RESOURCE, "{data.tablaPagareResource}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_LETRAS, "{data.datosRequest.totalLetras}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}");

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 930" + e.getMessage());
        }
        return result;
    }

    // doc988-servicio_iptv_fija
    private String editaDoc988(String plantilla) {
        String result = "";

        try {

            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "" + "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES, "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "" + "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_TOTAL_IPTV, "" + "{data.datosRequest.totalIptv}")
                    .replaceAll(MetadatosPDA.MD_TIPO_CLIENTE_TV, "{data.radioButtonCliente}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NUMERO, "{data.datosRequest.tarjetaNumero}")
                    .replaceAll(MetadatosPDA.MD_TARJETA_NOMBRE_CLIENTE, "{data.datosRequest.nombreTitularTarjeta}")
                    .replaceAll(MetadatosPDA.MD_SOLICITUD_ID, "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_PRICE_BEFORE_IPTV, "{data.datosRequest.servicioIpTv.beforePrice}")
                    .replaceAll(MetadatosPDA.MD_PRICE_AFTER_IPTV, "{data.datosRequest.servicioIpTv.afterPrice}")
                    .replaceAll(MetadatosPDA.MD_PLAN_ACT_IPTV, "{data.datosRequest.servicioIpTv.plan}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_TIEMPO_CLI_IPTV, "{data.datosRequest.perminTiempoCli}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_CLI_IPTV, "{data.radioButtonPerminCli}")
                    .replaceAll(MetadatosPDA.MD_PERMIN_BENEF_CLI_IPTV, "{data.permiCliIptv}")
                    .replaceAll(MetadatosPDA.MD_PAGO_FORMA_FIJO, "{data.radioButtonPagoFijo}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_TITULAR_BAN, "{data.datosRequest.nombreTitularCta}")
                    .replaceAll(MetadatosPDA.MD_DOM_PROVINCIA1, "{data.datosRequest.domProvinciaInst}")
                    .replaceAll(MetadatosPDA.MD_DOM_CANTON, "{data.datosRequest.domCantonInst}")
                    .replaceAll(MetadatosPDA.MD_DIRRECCION_INSTAL, "{data.datosRequest.direccionInstalacion}")
                    .replaceAll(MetadatosPDA.MD_CODIGO_VENDEDOR, "{data.datosRequest.codigoVendedor}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_CANALES_AUDIO_IPTV, "{data.datosRequest.canalesIncluidoAudio}")
                    .replaceAll(MetadatosPDA.MD_CANALES_VIDEO_IPTV, "{data.datosRequest.canalesIncluidoVideo}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NUMERO_CUENTA_BANCARIA, "{data.datosRequest.bancoNumeroCuenta}")
                    .replaceAll(MetadatosPDA.MD_BANCO_NOMBRE_COMPLETO, "{data.datosRequest.bancoNombreCompleto}")
                    .replaceAll(MetadatosPDA.MD_BEFORE_SUSC_IPTV,
                            "{data.datosRequest.pagoInstalacionIptv.beforePrice}")
                    .replaceAll(MetadatosPDA.MD_AFTER_SUSC_IPTV,
                            "{data.datosRequest.pagoInstalacionIptv.afterPrice}")
                    .replaceAll(MetadatosPDA.MD_ADICIONAL_IPTV, "{data.adicionalIpTv}");

        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 988" + e.getMessage());
        }
        return result;
    }

    // doc914-solicitud_financiamiento_venta_one
    private String editaDoc914(String plantilla) {
        String result = "";

        try {
            result = plantilla
                    .replaceAll(MetadatosPDA.MD_CLIENTE_IDENTIFICACION, "{data.datosRequest.clienteCedula}")
                    .replaceAll(MetadatosPDA.MD_BASE_FINANCIAMIENTO, "{data.datosRequest.valorBaseFinanciamiento}") // Formatear
                    .replaceAll(MetadatosPDA.MD_TRAMITE_DIA, "" + "{data.datosRequest.tramiteDia}")
                    .replaceAll(MetadatosPDA.MD_SUMA_VALORES, "{data.sumaValores1Formateada}") // Formatear con dos
                    .replaceAll(MetadatosPDA.MD_NUM_CUENTA_BSCS, "{data.datosRequest.numeroCuentaBSCS}")
                    .replaceAll(MetadatosPDA.MD_USUARIO_DIGITADOR, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_ID_OFIC, "{data.datosRequest.idOficina}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_NOMBRE, "{data.datosRequest.oficinaNombre}")
                    .replaceAll(MetadatosPDA.MD_OFICINA_CIUDAD, "{data.datosRequest.oficinaCiudad}")
                    .replaceAll(MetadatosPDA.MD_FECHA_ACTUAL, "{data.fechaFormateada}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_NUMERO, "" + "{data.datosRequest.idTransactionPDA}")
                    .replaceAll(MetadatosPDA.MD_VALOR_IVA_FINANCIAMIENTO, "{data.datosRequest.valorIvaFinanciamiento}") // Formatear
                    .replaceAll(MetadatosPDA.MD_CLIENTE_NOMBRE_COMPLETO,
                            "{data.datosRequest.clienteNombres.toUpperCase} {data.datosRequest.clientePrimerApellido.toUpperCase} {data.datosRequest.clienteSegundoApellido.toUpperCase}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_MES,
                            "{data.mesString}")
                    .replaceAll(MetadatosPDA.MD_TRAMITE_ANIO, "{data.datosRequest.tramiteAnio}")
                    .replaceAll(MetadatosPDA.MD_CUOTA_INICIAL, "{data.datosRequest.cuotaInicial}")
                    .replaceAll(MetadatosPDA.MD_NOMBRE_USUARIO, "{data.datosRequest.usuarioDigitador}")
                    .replaceAll(MetadatosPDA.MD_CLIENTE_SERVICIO, "{data.datosRequest.clienteServicio}")
                    .replaceAll(MetadatosPDA.MD_TABLA_DET_FIN_V_ONE, "{data.tablaDetalleFinanciamiento}")
                    .replaceAll(MetadatosPDA.MD_TABLA_CUOTAS_FIN, "{data.tablaCoutaFinanciamiento}")
            // .replaceAll(MetadatosPDA.MD_FINANCIAMIENTO, "{data.financiamiento}")
            // AQUI FALTA %%FINAN:ID%%
            ;
        } catch (RuntimeException e) {
            throw new RuntimeException("Error al actualizar la plantilla 914" + e.getMessage());
        }
        return result;
    }

    private String completarHtml(String plantilla) {
        String headerHTML = "<!DOCTYPE html>\n" + "<html>\n" + "   <head>\n" +
                " <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"/>\n" +
                "       <title></title>\n" + "       <style type=\"text/css\">\n" +
                "            body{ font-family: Arial, Helvetica, sans-serif; font-size: 11.8px;\n" +
                "            line-height: 1.05;  }\n" +
                "           .circulo { border-radius: 25px;border: 2px solid #555;padding: 1px 6px 1px;}\n" +
                "           .cuadrado { border: 2px solid #555;padding: 2px 6px 1px;}\n" +
                "           ul.listHorizontal{ margin:0;padding:0;list-style-type: none;}\n" +
                "           ul.listHorizontal li { margin:0;padding:0;display: inline;list-style-type: none;}\n" +
                "           ul.listVertical{ margin:0;padding:0;list-style-type: none;}\n" +
                "           ul.listVertical li { margin:5px auto;padding:0;list-style-type: none;}\n" +
                "           div.marco{ border:1px solid #000;width:100%;border-bottom-left-radius: 20px;border-bottom-right-radius: 5px; border-top-left-radius: 5px; border-top-right-radius: 20px;}\n"
                +
                "       </style>\n" + "   </head>\n" + "<body>";

        plantilla = plantilla.replaceAll("á", "a")
                .replaceAll("é", "e")
                .replaceAll("í", "i")
                .replaceAll("ó", "o")
                .replaceAll("ú", "u")
                .replaceAll("ñ", "n")
                .replaceAll("&nbsp;", "&#160;")
                .replaceAll(MetadatosPDA.MD_CLIENTE_FIRMA, "<div style=\"height: 80px;\"></div>")
                .replaceAll(MetadatosPDA.MD_LOGO_CLARO, "{data.logoClaro}")
                .replaceAll(MetadatosPDA.MD_LOGO_CARTILLA, "{data.logoClaro}");

        String footerHTML = "</body>\n" + "</html>";
        return String.format("%s%s%s", headerHTML, plantilla, footerHTML);
    }

}