
package net.sasf.service.contract;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.NoResultException;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;

import net.sasf.dto.DocumentResponse;
import net.sasf.dto.DtoDatosPda;
import net.sasf.dto.MergedPdfResponse;
import net.sasf.model.DatosPDA;
import net.sasf.service.EventLogService;
import net.sasf.utils.TemplateContext;

@Singleton
public class ContractService {

    @Inject
    private DocumentGeneratorService pdfGenerator;

    @Inject
    private EventLogService eventLogService;

    public MergedPdfResponse generateContract(DatosPDA datosPDA) {

        List<DocumentResponse> documents = new ArrayList<>();
        TemplateContext<DtoDatosPda> ctx = new TemplateContext<>();
        DtoDatosPda datos = new DtoDatosPda();
        datos.setDatosRequest(datosPDA);

        ctx.setData(datos);

        // String forma_pago = configProperties.getProperty("forma_pago")
        // .trim()
        // .toUpperCase();

        String status = "";
        /* Validaci�n de los datos */
        if (datosPDA == null) {
            status = "Datos vacios";
        } else {
            status = datos.getDatosRequest().verificaIntegridad("EFECTIVO");
        }

        if (!status.equals("")) {

            eventLogService.saveLog(datosPDA.getRequestId(), "Datos vacios.",
                        "El documento SIGNED_" + datosPDA.getIdTransactionPDA() + "_v1.pdf llegó con datos vacios: " + status,
                        "", "E");

            throw new RuntimeException("Error en los datos: " + status);
        }

        // Oferta fijo 0 y Recurso fijo 0
        if (datos.getDatosRequest().getOfertaFijo() != 1 &&
                datos.getDatosRequest().getRecursosFijo() != 1) {

            if ((datos.getDatosRequest().getInternetFijo() == 1 ||
                    datos.getDatosRequest().getTelefoniaFijo() == 1 || datos.getDatosRequest().getTvFijo() == 1)) {

                // Contrato
                if (datos.getDatosRequest().getNewClien() == 1) {

                    documents.add(new DocumentResponse(
                            "941.pdf", "PDF",
                            this.pdfGenerator.generateFile("941", ctx , datosPDA.getRequestId())));
                }
                // aceptacion_promocion
                if (datos.getDatosRequest().getIpTv() == 0 &&
                        datos.getDatosRequest().getOtt() == 1) {
                    documents.add(new DocumentResponse(
                            "991.pdf", "PDF",
                            this.pdfGenerator.generateFile("991", ctx , datosPDA.getRequestId())));
                }

            }

            // tiene contratado la television HFC
            // anexo_tv
            if (datos.getDatosRequest().getTvFijo() == 1) {
                documents.add(new DocumentResponse(
                        "970.pdf", "PDF",
                        this.pdfGenerator.generateFile("970", ctx , datosPDA.getRequestId())));
            }

            // tiene contratado el internet gpon
            // anexo_internet
            if (datos.getDatosRequest().getInternetFijo() == 1) {
                documents.add(new DocumentResponse(
                        "1105.pdf", "PDF",
                        this.pdfGenerator.generateFile("1105", ctx , datosPDA.getRequestId())));
            }

            // tiene contratado la telefonia gpon
            // anexo_telefonia
            if (datos.getDatosRequest().getTelefoniaFijo() == 1) {
                documents.add(new DocumentResponse(
                        "1106.pdf", "PDF",
                        this.pdfGenerator.generateFile("1106", ctx , datosPDA.getRequestId())));
            }

            // Tiene los servicios completos
            if (datos.getDatosRequest().getInternetFijo() == 1 &&
                    datos.getDatosRequest().getTelefoniaFijo() == 1 &&
                    datos.getDatosRequest().getTvFijo() == 1) {

                // autorizacion_debito
                if (!datos.getDatosRequest().getFormaPago1().toUpperCase().equals("FORMA_PAGO")) {
                    documents.add(new DocumentResponse(
                            "24.pdf", "PDF",
                            this.pdfGenerator.generateFile("24", ctx , datosPDA.getRequestId())));
                }
                // factura_electronica
                documents.add(new DocumentResponse(
                        "956.pdf", "PDF",
                        this.pdfGenerator.generateFile("956", ctx , datosPDA.getRequestId())));
            }

        }
        // Oferta fijo 1
        if (datos.getDatosRequest().getOfertaFijo() == 1) {

            // oferta_suplementaria
            for (int i = 0; i < datos.getDatosRequest().getOfertas().length; i++) {

                datos.setOfertasRecursos(datos.getDatosRequest().getOfertas()[i]);

                documents.add(new DocumentResponse(
                        "oferta-suplementaria-952-" + i + ".pdf", "PDF",
                        this.pdfGenerator.generateFile("952", ctx , datosPDA.getRequestId())));

            }

        }
        // Recurso fijo 1
        if (datos.getDatosRequest().getRecursosFijo() == 1) {
            // recurso_suplementaria
            for (int i = 0; i < datos.getDatosRequest().getRecursos().length; i++) {

                datos.setOfertasRecursos(datos.getDatosRequest().getRecursos()[i]);

                documents.add(new DocumentResponse(
                        "recurso-fijo-952-" + i + ".pdf", "PDF",
                        this.pdfGenerator.generateFile("952", ctx , datosPDA.getRequestId())));
            }

            // financiamiento si es gpon, hfc ok
            if (!(datos.getDatosRequest().getFinanciamiento() == null
                    || datos.getDatosRequest().getFinanciamiento().equals(""))) {

                if (datos.getDatosRequest().getIpTv() == 1 || datos.getDatosRequest().getOtt() == 1) {

                    // solicitud_financiamiento_venta_one
                    documents.add(new DocumentResponse(
                            "914.pdf", "PDF",
                            this.pdfGenerator.generateFile("914", ctx , datosPDA.getRequestId())));
                    // pagare_orden_hwi
                    documents.add(new DocumentResponse(
                            "930.pdf", "PDF",
                            this.pdfGenerator.generateFile("930", ctx , datosPDA.getRequestId())));

                } else {

                    // financiamiento
                    documents.add(new DocumentResponse(
                            "233.pdf", "PDF",
                            this.pdfGenerator.generateFile("233", ctx , datosPDA.getRequestId())));
                    // pagare
                    documents.add(new DocumentResponse(
                            "946.pdf", "PDF",
                            this.pdfGenerator.generateFile("946", ctx , datosPDA.getRequestId())));

                }
            }
        }
        // aceptacion_promocion
        if (datos.getDatosRequest().getTecnologia().equals("GPON") &&
                (datos.getDatosRequest().getIpTv() == 1 || datos.getDatosRequest().getOtt() == 1)) {
            // aceptacion_promocion
            if (datos.getDatosRequest().getIpTv() == 1) {
                documents.add(new DocumentResponse(
                        "991.pdf", "PDF",
                        this.pdfGenerator.generateFile("991", ctx , datosPDA.getRequestId())));
            }
            // aceptacion_promocion_ott
            if (datos.getDatosRequest().getOtt() == 1) {
                documents.add(new DocumentResponse(
                        "989.pdf", "PDF",
                        this.pdfGenerator.generateFile("989", ctx , datosPDA.getRequestId())));
            }
        }
        // aceptacion_promocion_nuevo
        if (datos.getDatosRequest().getTecnologia().equals("HFC") &&
                (datos.getDatosRequest().getIpTv() == 1 || datos.getDatosRequest().getOtt() == 1)) {
            // aceptacion_promocion_nuevo
            if (datos.getDatosRequest().getIpTv() == 1) {
                documents.add(new DocumentResponse(
                        "991.pdf", "PDF",
                        this.pdfGenerator.generateFile("991", ctx , datosPDA.getRequestId())));
            }
            // aceptacion_promocion_ott
            if (datos.getDatosRequest().getOtt() == 1) {
                documents.add(new DocumentResponse(
                        "989.pdf", "PDF",
                        this.pdfGenerator.generateFile("989", ctx , datosPDA.getRequestId())));
            }

        }
        // iptv 1 y ott 1
        if (datos.getDatosRequest().getIpTv() == 1 || datos.getDatosRequest().getOtt() == 1) {

            if (datos.getDatosRequest().getIpTv() == 1) {
                // servicio_iptv_fija
                documents.add(new DocumentResponse(
                        "988.pdf", "PDF",
                        this.pdfGenerator.generateFile("988", ctx , datosPDA.getRequestId())));
                // if (plantillas.containsKey(anexo_condiciones)) {
                // log.info("anexo_condiciones: " + anexo_condiciones);
                // docHTML = plantillas.get(anexo_condiciones).getContenido();
                // documentos.put(contador++, docHTML);
                // }
            }

            if (datos.getDatosRequest().getOtt() == 1) {
                // tirilla_contratacion_oferta
                documents.add(new DocumentResponse(
                        "1167.pdf", "PDF",
                        this.pdfGenerator.generateFile("1167", ctx , datosPDA.getRequestId())));
            }

            if (datos.getDatosRequest().getNewClien() == 1) {
                // ternimo_condiciones
                documents.add(new DocumentResponse(
                        "985.pdf", "PDF",
                        this.pdfGenerator.generateFile("985", ctx , datosPDA.getRequestId())));

                // renta_equipo
                documents.add(new DocumentResponse(
                        "981.pdf", "PDF",
                        this.pdfGenerator.generateFile("981", ctx , datosPDA.getRequestId())));
            }

        }

        if (datos.getDatosRequest().getFmc() == 1) {
            // fmc
            documents.add(new DocumentResponse(
                    "1030.pdf", "PDF",
                    this.pdfGenerator.generateFile("1030", ctx , datosPDA.getRequestId())));
        }

        if (datos.getDatosRequest().getNewClien() == 1) {
            // anexo_privacidad_clientes
            documents.add(new DocumentResponse(
                    "1040.pdf", "PDF",
                    this.pdfGenerator.generateFile("1040", ctx , datosPDA.getRequestId())));
        }

        if (documents.size() != 0) {
            ByteArrayOutputStream mergedPdfStream = new ByteArrayOutputStream();
            Document mergedPdfDocument = new Document();

            try {
                PdfCopy copy = new PdfCopy(mergedPdfDocument, mergedPdfStream);
                mergedPdfDocument.open();

                for (DocumentResponse document : documents) {
                    PdfReader reader = new PdfReader(document.getDocument());
                    int numberOfPages = reader.getNumberOfPages();

                    for (int page = 1; page <= numberOfPages; page++) {
                        copy.addPage(copy.getImportedPage(reader, page));
                    }
                }

                mergedPdfDocument.close();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Error al combinar los documentos PDF.");
            }

            return new MergedPdfResponse(mergedPdfStream.toByteArray());
        } else {
            throw new NoResultException("No se generaron documentos");
        }

    }

}
