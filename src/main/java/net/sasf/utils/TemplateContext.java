
package net.sasf.utils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateContext<T> {
    
    private T data;

}
