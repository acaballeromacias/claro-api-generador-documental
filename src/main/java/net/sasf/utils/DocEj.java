package net.sasf.utils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocEj {
    private double valor;
    
    //debo retornar este valor formateado con dos decimales 
    public double getValor() {
        return valor;
    }
}
