package net.sasf.utils;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.TransactionRequiredException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.hibernate.exception.DataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;

import net.sasf.utils.model.ErrorDetalles;

@Provider
public class ManejadorExcepciones implements ExceptionMapper<Exception>{
	
	@Inject
	private javax.inject.Provider<ContainerRequestContext> containerRequestContextProvider;
	
	private static Logger logger = LoggerFactory.getLogger(ManejadorExcepciones.class);
    
    public static Response error(Integer statusCode, Exception e, String mensaje) {
    	logger.error("Exception : {}", e != null ? e.getMessage() : "");
    	logger.error("Exception completa", e);
    	
    	var errorDetails = new ErrorDetalles(new Date(), mensaje,
    			e != null ? e.getMessage() : "", statusCode);
    	
        return Response.status(statusCode).entity(errorDetails).build();
    }

	@Override
	public Response toResponse(Exception exception) {
		return mapExceptionToResponse(exception);
	}
	
	private Response mapExceptionToResponse(Exception exception) {
	    if (exception instanceof RuntimeException) {
	    	if (exception.fillInStackTrace() instanceof DataException) {
	  		      return error(Status.BAD_REQUEST.getStatusCode(),exception,exception.getMessage());
	    	}
	    	else if (exception instanceof NoResultException) {
			      return error(Status.BAD_REQUEST.getStatusCode(),exception,exception.getMessage());
			}
		    else if (exception instanceof IllegalArgumentException) {
		      return error(Status.BAD_REQUEST.getStatusCode(),exception,exception.getMessage());
		    }
		    else if (exception instanceof EntityExistsException) {
			      return error(Status.BAD_REQUEST.getStatusCode(),exception,exception.getMessage());
		    }
		    else if (exception instanceof TransactionRequiredException) {
			      return error(Status.INTERNAL_SERVER_ERROR.getStatusCode(),exception,exception.getMessage());
		    } 
		    else if(exception.getCause()!=null) {
	    		
		    	if (exception.getCause() instanceof ConstraintViolationException) {
		            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) exception.getCause()).getConstraintViolations();
		            Set<String> messages = new HashSet<>(constraintViolations.size());
		        	messages.addAll(constraintViolations.stream()
		        	        .map(constraintViolation -> String.format("%s", constraintViolation.getMessage()))
		        	        .collect(Collectors.toList()));
	
		        	return error(Status.BAD_REQUEST.getStatusCode(), exception, messages.toString());
		    	}
		    	else if (exception.getCause().getCause() instanceof ConstraintViolationException) {
		            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) exception.getCause().getCause()).getConstraintViolations();
		            Set<String> messages = new HashSet<>(constraintViolations.size());
		        	messages.addAll(constraintViolations.stream()
		        	        .map(constraintViolation -> String.format("%s", constraintViolation.getMessage()))
		        	        .collect(Collectors.toList()));
	
		        	return error(Status.BAD_REQUEST.getStatusCode(), exception, messages.toString());
		    	}
	    	}
	        return error(Status.INTERNAL_SERVER_ERROR.getStatusCode(), exception, exception.getMessage());
	    }
		else if (exception instanceof JsonParseException) {
			return error( Status.BAD_REQUEST.getStatusCode(), exception, "Formato incorrecto en el cuerpo de la petición");
		}
		else if (exception instanceof IOException) {
        	return error(Status.INTERNAL_SERVER_ERROR.getStatusCode(), exception, "Error de E/S durante la operación.");
    	}
	    else {
	      logger.error("Failed to process request to: {}. Exception is: {}",
	                    containerRequestContextProvider.get().getUriInfo(), exception);
	      var errorDetails = new ErrorDetalles(new Date(), "Internal Server Error",
	    		  "Internal Server Error", Status.INTERNAL_SERVER_ERROR.getStatusCode());
	      return Response.serverError().entity(errorDetails).build();
	    }
	  }
}
