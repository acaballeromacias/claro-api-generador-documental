package net.sasf.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;

import net.sasf.utils.model.ClientSettings;


public class ConfigLoader {

    private ClientSettings settings;
    private final String CONFIGURATION_FILE_LOCATION = "src/main/java/net/sasf/utils/conf/settings.json";

    private static ConfigLoader confLoader;
    
    private ConfigLoader() {
        
        try (FileReader fr = new FileReader(this.CONFIGURATION_FILE_LOCATION)) {

            Gson gson = new Gson();
            this.settings = gson.fromJson(
                new BufferedReader(fr), 
                ClientSettings.class
            );

        } catch (IOException e) {
            e.printStackTrace(); }    

    }

    public static ClientSettings GetSettings() {

        return (
            (confLoader == null) ? new ConfigLoader().settings : confLoader.settings
        );

    }

}
