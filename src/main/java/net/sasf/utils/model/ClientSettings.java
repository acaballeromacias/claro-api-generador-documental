package net.sasf.utils.model;

import lombok.Getter;

@Getter
public class ClientSettings {

    private String BASE_URL;
    private String UPLOAD_DOCUMENTS;
    private String GENERATE_TOKEN;
    private Boolean USE_SSL;
    private Boolean NOSESSION;

    private String USERNAME;
    private String PASSWORD;
}
