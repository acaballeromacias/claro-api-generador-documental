package net.sasf.utils.model;

import java.util.Date;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@RegisterForReflection
public class ErrorDetalles {

	private Date timestamp;
	private String message;
	private String details;
	private Integer status;
}
