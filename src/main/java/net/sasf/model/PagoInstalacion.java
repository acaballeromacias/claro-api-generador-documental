package net.sasf.model;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PagoInstalacion {
    private String detalle;
    private String cantidad;
    private String afterPrice;
    @Pattern(regexp = "^[0-9]*$", message = "'pagoInstalacion -> beforePrice' debe tener solamente caracteres NÚMERICOS")
    private String beforePrice;
}
