package net.sasf.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class EventLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "El campo RequestId no puede ser nulo.")
    private String requestId;

    @NotNull(message = "La descripción no puede ser nula.")
    private String descripcion;

    @NotNull(message = "El detalle no puede ser nulo.")
    private String detalle;

    private String ruta;

    @NotNull(message = "La fecha creación no puede ser nula.")
    private LocalDateTime fechaCreacion;

    @NotNull(message = "El tipo no puede ser nulo.")
    @Size(min = 1, max = 1, message = "El tipo debe tener exactamente un carácter.")
    @Pattern(regexp = "[IFEN]", message = "El tipo debe ser 'I', 'F', 'N' o 'E'.")
    private String tipo;
}
