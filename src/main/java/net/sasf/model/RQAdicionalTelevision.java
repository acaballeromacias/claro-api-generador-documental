package net.sasf.model;

import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RQAdicionalTelevision {
    private String plan;
    private String numero;
    private Integer cantidad;
    private String afterPrice;
    @Pattern(regexp = "^[0-9]*$", message = "'adicionalTelevision -> beforePrice' debe tener solamente caracteres NÚMERICOS")
    private String beforePrice;
    private int recurringFlag;
}
