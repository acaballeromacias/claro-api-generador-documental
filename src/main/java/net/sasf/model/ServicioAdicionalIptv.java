package net.sasf.model;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServicioAdicionalIptv {
    private String plan;
    private String cantidad;
    private String afterPrice;
    @Pattern(regexp = "^[0-9]*$", message = "'servicioAdicionalIptv -> beforePrice' debe tener solamente caracteres NÚMERICOS")
    private String beforePrice;
    private int recurringFlag;
}
