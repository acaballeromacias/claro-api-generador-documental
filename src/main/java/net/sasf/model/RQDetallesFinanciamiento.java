package net.sasf.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RQDetallesFinanciamiento {
    private String numFactura;
    private String sriFact;
    private String fechaFact;
    private String usuarioFact;
    private String marcaEquipo;
    private String valor;
}
 