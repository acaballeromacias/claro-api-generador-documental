package net.sasf.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Cliente{

    private Long id;

    @NotNull(message = "La dirección no puede ser nula")
    private String address;

    @NotNull(message = "La provincia no puede ser nula")
    private String province;

    @NotNull(message = "La ciudad no puede ser nula")
    private String city;

    @NotNull(message = "El país no puede ser nulo")
    private String country;

    @NotNull(message = "El nombre no puede ser nulo")
    private Integer givenName;

    private String secondName;

    @NotNull(message = "El primer apellido no puede ser nulo")
    private String surname1;

    private String surname2;

    @Pattern(regexp = "\\d{10}", message = "El NUI debe tener 10 dígitos")
    @NotNull(message = "El NUI no puede ser nulo")
    private String nui;

    @Email(message = "El correo electrónico debe ser válido")
    @NotNull(message = "El correo electrónico no puede ser nulo")
    private String email;

    @Pattern(regexp = "\\d{10}", message = "El número de teléfono debe tener 10 dígitos")
    @NotNull(message = "El número de teléfono no puede ser nulo")
    private String phoneNumber;

    @NotNull(message = "La razón no puede ser nulo")
    private String reason;

    @NotNull(message = "Debe existir una orden de compra")
    private String orderId;

    @NotNull(message = "El tipo de vendedor no puede ser nulo")
    private String typeSeller;

    @NotNull(message = "El vendedor no puede ser nulo")
    private String seller;

    // private byte[] file;

    private String businessName;

    private String branch;

    private String contractedPlan;

    private String ids;

    private Integer age;

}
