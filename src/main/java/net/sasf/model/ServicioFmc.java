package net.sasf.model;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServicioFmc {
    private String servicio;
    @Pattern(regexp = "^[0-9]*$", message = "'servicioFmc -> valor' debe tener solamente caracteres NÚMERICOS")
    private String valor;

}
