package net.sasf.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RQTablaPagareResource {
    private String concepto;
    private String cuota;
    private String usvCouta;
    private String fechaPago;
    private String cuotaLetra;
}
