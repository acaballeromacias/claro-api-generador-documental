package net.sasf.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TirillaIptv {
    private String noSuscripcion;
    private String tipoSuscripcion;
    private String observacionAsesor;
    private String totalOtt;
    private String ofertaNueva;
    private String correoElectronico;
}
