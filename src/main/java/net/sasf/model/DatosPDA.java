package net.sasf.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DatosPDA {
    private String orderId;
    @JsonProperty("IdTransactionPDA")
    @NotBlank(message = "idTransactionPDA no puede ser vacío")
    @Pattern(regexp = "^[0-9]*$", message = "idTransactionPDA debe tener solamente caracteres NÚMERICOS")
    private String idTransactionPDA;
    private String requestId;
    private int tramiteDia;
    private int tramiteMes;
    private int tramiteAnio;
    private String idOficina;
    private String oficinaNombre;
    private String oficinaCiudad;
    private int region;
    private String usuarioDigitador;
    private int tipoCliente;
    private String clientePrimerApellido;
    private String clienteSegundoApellido;
    private String clienteNombres;
    private String clienteFechaNacimiento;
    private String clienteNacionalidad;
    private String clienteTipoIdentificacion;
    private String clienteCedula;
    private int personaTipoCF;
    private String clienteServicio;
    private String personaEmail;
    private String domicilioDirCompleta;
    private String domicilioTelefono;
    private String domicilioTelefono1;
    private String viviendaEntrada;
    private String direccionOrden;
    private String resourceFactura;
    private RQAdicionalInternet[] adicionalInternet;
    private String ivaInternet;
    private RQCondicionesInternet[] condicionesInternet;
    private RQAdicionalTelefonia[] adicionalTelefonia;
    private RQAdicionalTelevision[] adicionalTelevision;
    private String formaPago1;
    private String direccionInstalacion;
    private String bancoNombreCompleto;
    private String tarjetaTipoNombre;
    private String tarjetaBancoNombre;
    private String tarjetaNumero;
    private String bancoNumeroCuenta;

    private int internetFijo; // 0=No, 1=Si
    private int telefoniaFijo; // 0=No, 1=Si
    private int ofertaFijo; // 0=No, 1=Si*
    private int tvFijo; // 0=No, 1=Si
    private int recursosFijo; // 0=No, 1=Si

    private String planActualTelefonia;
    @Pattern(regexp = "^[0-9]*$", message = "priceBeforeTelefonia debe tener solamente caracteres NÚMERICOS")
    private String priceBeforeTelefonia;
    private String planActualInternet;
    @Pattern(regexp = "^[0-9]*$", message = "priceBeforeInternet debe tener solamente caracteres NÚMERICOS")
    private String priceBeforeInternet;
    private String plantv;
    @Pattern(regexp = "^[0-9]*$", message = "priceBeforeTv debe tener solamente caracteres NÚMERICOS")
    private String priceBeforeTv;
    private String imgClienteFirma;
    private String imgCedulaCliente1;
    private String imgCedulaCliente2;
    private String imgFotoLugar;
    private String imgPlanillaSB;
    private String imgFacturaOtros;
    private String domProvinciaInst;
    private String domCiudadInst;
    private String domCantonInst;
    private String domParroqInst;
    private String domCallePInst;
    private String domCalleSInst;
    private String domNumeracInst;
    private String domEdifInst;
    private String domPisoInst;
    private String domOficInst;
    private String domRefInst;
    private String direccInstalacion;
    private String perminCli;
    private String perminTiempoCli;
    private String perminBenefCli;
    private String canalesAudioTv;
    private String canalesVideoTv;
    private String clienteDiscap;
    private String clienteIdent;
    private String nombreUser;
    private String pagoFormaFijo;
    private String terceraEdad;
    private String priceAfterInt;
    private String afterSuscInt;
    @Pattern(regexp = "^[0-9]*$", message = "beforeSuscInt debe tener solamente caracteres NÚMERICOS")
    private String beforeSuscInt;
    private String priceAfterTel;
    @Pattern(regexp = "^[0-9]*$", message = "beforeSuscTel debe tener solamente caracteres NÚMERICOS")
    private String beforeSuscTel;
    private String afterSuscTel;
    private String priceAfterTv;
    @Pattern(regexp = "^[0-9]*$", message = "beforeSuscTv debe tener solamente caracteres NÚMERICOS")
    private String beforeSuscTv;
    private String afterSuscTv;
    private String totalIntImp;
    private String totalTelImp;
    private String totalTvImp;
    private String velocidadMin;
    private String velocidadMax;
    private String minutosOffnet;
    private String minutosOnnet;
    private String nombreTitularTarjeta;
    private String nombreTitularCta;
    private double sumaValores2;
    private double cuotaInicial;
    private String financiamiento;
    private String numeroCuentaBSCS;
    private RQDetallesFinanciamiento[] tablaFinanciamientoSGA;
    private double valorBaseFinanciamiento;
    private double valorIvaFinanciamiento;
    private double sumaValores1;
    private RQTablaCuotaFinanciamiento[] tablaCuotasFinanciamiento;
    private String totalLetras;
    private RQTablaPagareResource[] tablaPagareResource;
    private String numeroCuentaFacturacion;
    private int totalIntImpDouble;
    private int totalTelImpDouble;
    private float totalTvImpDouble;
    private boolean internet;
    private boolean telefonia;
    private boolean tv;
    private int newClien;
    private int ipTv;
    private int ott;
    private ServicioAdicionalIptv[] adicionalIpTv;
    private ServicioIptv servicioIpTv;
    private String modalidadPrestacion;
    private String tipoClienteTV;
    private int canalesIncluidoAudio;
    private int canalesIncluidoVideo;
    private TirillaIptv iptvTirilla;
    private TirillaIptv ottTirilla;
    private PagoInstalacion pagoInstalacionIptv;
    private String tecnologia;
    private PagoInstalacion pagoInstalacionOtt;
    private String totalIptv;
    private String totalOtt;
    private String hdTvRentaEquiposPr;
    private String hdPrecioRentaEquipo;
    private String sdTvRentaEquiposAd;
    private String sdPrecioRentaEquipo;
    private String codigoVendedor;
    private RQOfertas[] ofertas;
    private RQOfertas[] recursos;
    private int fmc;
    @Pattern(regexp = "^[0-9]*$", message = "priceBeforeFmc debe tener solamente caracteres NÚMERICOS")
    private float priceBeforeFmc;
    private float priceAfterFmc;
    private String paqueteFmc;
    private ServicioFmc[] detalleFmc;
    private String tamanioFmc;
    private String cabeceraFmc;

    private String valorDepositoGarantiaInt;
    private String depositoGarantiaInt;

    public String siNo(String siNo) {

        String result = "";

        switch (siNo) {

            case "S":
                result = "Si";
                break;
            case "N":
                result = "No";
                break;
            default:
                result = "";
        }

        return result;
    }

    public String verificaIntegridad(String formaPagoValida) {

        // SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        /* Variables para control de errores */
        String status = "";

        if (valorDepositoGarantiaInt == null || valorDepositoGarantiaInt.isEmpty()) {
            valorDepositoGarantiaInt = "0.00";
        }

        if (depositoGarantiaInt == null || depositoGarantiaInt.isEmpty()) {
            depositoGarantiaInt = "N";
        }

        // tecnologia
        if (tecnologia == null || tecnologia.isEmpty()) {
            status += "tecnologia";
        }

        if (idTransactionPDA == null || idTransactionPDA.isEmpty()) {
            status += "idTransactionPDA";
        }
        if (tramiteDia < 1 || tramiteDia > 31) {
            status += "|tramiteDia";
        }
        if (tramiteMes < 1 || tramiteMes > 12) {
            status += "|tramiteMes";
        }
        if (tramiteAnio < 1) {
            status += "|tramiteAnio";
        }

        if (ofertaFijo != 1) {
            if (formaPago1 == null || formaPago1.isEmpty()) {
                status += "|formaPago1";
            }

            if (!(formaPago1.toUpperCase().equals(formaPagoValida))) {
                if (!(pagoFormaFijo == null)) {
                    if (pagoFormaFijo.equals("A") || pagoFormaFijo.equals("C")) {
                        if (bancoNumeroCuenta == null || bancoNumeroCuenta.isEmpty()) {
                            status += "|bancoNumeroCuenta";
                        }
                        tarjetaTipoNombre = " ";
                        tarjetaBancoNombre = " ";
                        tarjetaNumero = " ";
                    }
                    if (pagoFormaFijo.equals("T")) {
                        if (tarjetaBancoNombre == null || tarjetaBancoNombre.isEmpty()) {
                            status += "|tarjetaBancoNombre";
                        }
                        if (bancoNombreCompleto == null || bancoNombreCompleto.isEmpty()) {
                            bancoNombreCompleto = tarjetaBancoNombre;
                        }
                        if (tarjetaTipoNombre == null || tarjetaTipoNombre.isEmpty()) {
                            status += "|tarjetaTipoNombre";
                        }
                        if (tarjetaNumero == null || tarjetaNumero.isEmpty()) {
                            status += "|tarjetaNumero";
                        }
                        bancoNumeroCuenta = " ";
                    }
                }

                if (bancoNombreCompleto == null || bancoNombreCompleto.isEmpty()) {
                    status += "|bancoNombreCompleto";
                }
            }
        }

        if (internetFijo == 1 || telefoniaFijo == 1 || tvFijo == 1) {

            if (codigoVendedor == null || codigoVendedor.isEmpty()) {
                codigoVendedor = usuarioDigitador;
            }
            if (domCallePInst == null || domCallePInst.isEmpty()) {
                status += "|domCallePInst";
            }

            if (domProvinciaInst == null || domProvinciaInst.isEmpty()) {
                status += "|domProvinciaInst";
            }

            if (domCiudadInst == null || domCiudadInst.isEmpty()) {
                status += "|domCiudadInst";
            }

            if (domCantonInst == null || domCantonInst.isEmpty()) {
                status += "|domCantonInst";
            }

            if (domParroqInst == null || domParroqInst.isEmpty()) {
                status += "|domParroqInst";
            }

            if (domCalleSInst == null || domCalleSInst.isEmpty()) {
                status += "|domCalleSInst";
            }

            if (domNumeracInst == null || domNumeracInst.isEmpty()) {
                status += "|domNumeracInst";
            }

            if (domEdifInst == null || domEdifInst.isEmpty()) {
                status += "|domEdifInst";
            }

            if (domPisoInst == null || domPisoInst.isEmpty()) {
                status += "|domPisoInst";
            }

            if (domOficInst == null || domOficInst.isEmpty()) {
                status += "|domOficInst";
            }

            if (domRefInst == null || domRefInst.isEmpty()) {
                status += "|domRefInst";
            }

            if (perminCli == null || perminCli.isEmpty()) {
                status += "|perminCli";
            } else if (!(perminCli.equals("S") || perminCli.equals("N"))) {
                status += "|perminCli debe ser S o N";
            }

            if (perminBenefCli == null || perminBenefCli.isEmpty()) {
                status += "|perminBenefCli";
            }

            if (clienteDiscap == null || clienteDiscap.isEmpty()) {
                status += "|clienteDiscap";
            } else if (!(clienteDiscap.equals("S") || clienteDiscap.equals("N"))) {
                status += "|clienteDiscap debe ser S o N";
            }

            if (numeroCuentaBSCS == null || numeroCuentaBSCS.isEmpty()) {
                status += "|numeroCuentaBSCS";
            }

            if (pagoFormaFijo == null) {
                status += "|pagoFormaFijo";
            } else if (!(pagoFormaFijo.equals("T")
                    || pagoFormaFijo.equals("C")
                    || pagoFormaFijo.equals("A")
                    || pagoFormaFijo.equals("F")
                    || pagoFormaFijo.equals(""))) {
                status += "|pagoFormaFijo debe ser T, C, A, F o \"\"";
            }

            if (terceraEdad == null || terceraEdad.isEmpty()) {
                status += "|terceraEdad";
            } else if (!(terceraEdad.equals("S")
                    || terceraEdad.equals("N"))) {
                status += "|terceraEdad debe ser S o N";
            }

            if (this.oficinaCiudad == null || this.oficinaCiudad.isEmpty()) {
                status += "|oficinaCiudad";
            }

            if (clienteNacionalidad == null || clienteNacionalidad.isEmpty()) {
                status += "|clienteNacionalidad";
            }

            if (domicilioDirCompleta == null || domicilioDirCompleta.isEmpty()) {
                status += "|domicilioDirCompleta";
            }
        } /*
           * else{
           * status += "|internetFijo o telefoniaFija o TvFija No debe ir 0";
           * }
           */

        if (internetFijo == 1) {

            if (adicionalInternet != null) {
                for (int i = 0; i < adicionalInternet.length; i++) {
                    if (adicionalInternet[i].getPlan() == null ||
                            adicionalInternet[i].getPlan().isEmpty()) {
                        status += "|adicionalInternet/Plan";
                    }
                    if (adicionalInternet[i].getCantidad() <= 0) {
                        status += "|adicionalInternet/cantidad";
                    }
                    if (adicionalInternet[i].getBeforePrice() == null ||
                            adicionalInternet[i].getBeforePrice().isEmpty()) {
                        status += "|adicionalInternet/beforePrice";
                    }
                    if (adicionalInternet[i].getAfterPrice() == null ||
                            adicionalInternet[i].getAfterPrice().isEmpty()) {
                        status += "|adicionalInternet/afterPrice";
                    }
                    /*
                     * if (String.valueOf(adicionalInternet[i].getRecurringFlag()) == null ||
                     * String.valueOf(adicionalInternet[i].getRecurringFlag()).isEmpty()) {
                     * status += "|adicionalInternet/recurringFlag";
                     * }
                     */
                }
            }
            if (priceBeforeInternet == null || priceBeforeInternet.isEmpty()) {
                status += "|priceBeforeInternet";
            }

            if (beforeSuscInt == null || beforeSuscInt.isEmpty()) {
                status += "|beforeSuscInt";
            }

            if (velocidadMin == null || velocidadMin.isEmpty()) {
                status += "|velocidadMin";
            }

            if (velocidadMax == null || velocidadMax.isEmpty()) {
                status += "|velocidadMax";
            }

            if (totalIntImp == null || totalIntImp.isEmpty()) {
                status += "|totalIntImp";
            }

            if (ivaInternet == null || ivaInternet.isEmpty()) {
                status += "|ivaInternet";
            }

        }

        if (tvFijo == 1) {
            if (adicionalTelevision != null) {
                for (int i = 0; i < adicionalTelevision.length; i++) {
                    if (adicionalTelevision[i].getPlan() == null ||
                            adicionalTelevision[i].getPlan().isEmpty()) {
                        status += "|adicionalTelevision/Plan";
                    }
                    if (adicionalTelevision[i].getCantidad() == null) {
                        status += "|adicionalTelevision/cantidad";
                    }
                    if (adicionalTelevision[i].getBeforePrice() == null ||
                            adicionalTelevision[i].getBeforePrice().isEmpty()) {
                        status += "|adicionalTelevision/beforePrice";
                    }
                    if (adicionalTelevision[i].getAfterPrice() == null ||
                            adicionalTelevision[i].getAfterPrice().isEmpty()) {
                        status += "|adicionalTelevision/afterPrice";
                    }
                    /*
                     * if (String.valueOf(adicionalTelevision[i].getRecurringFlag()) == null ||
                     * String.valueOf(adicionalTelevision[i].getRecurringFlag()).isEmpty()) {
                     * status += "|adicionalTelevision/recurringFlag";
                     * }
                     */
                }
            }
            if (priceBeforeTv == null || priceBeforeTv.isEmpty()) {
                status += "|priceBeforeTv";
            }

            if (beforeSuscTv == null || beforeSuscTv.isEmpty()) {
                status += "|beforeSuscTv";
            }

            if (totalTvImp == null || totalTvImp.isEmpty()) {
                status += "|totalTvImp";
            }

            if (canalesAudioTv == null || canalesAudioTv.isEmpty()) {
                status += "|canalesAudioTv";
            }

            if (canalesVideoTv == null || canalesVideoTv.isEmpty()) {
                status += "|canalesVideoTv";
            }
        }

        if (telefoniaFijo == 1) {

            if (adicionalTelefonia != null) {
                for (int i = 0; i < adicionalTelefonia.length; i++) {
                    if (adicionalTelefonia[i].getPlan() == null ||
                            adicionalTelefonia[i].getPlan().isEmpty()) {
                        status += "|adicionalTelefonia/Plan";
                        break;
                    }
                }
            }
            if (priceBeforeTelefonia == null || priceBeforeTelefonia.isEmpty()) {
                status += "|priceBeforeTelefonia";
            }

            if (beforeSuscTel == null || beforeSuscTel.isEmpty()) {
                status += "|beforeSuscTel";
            }

            if (minutosOffnet == null || minutosOffnet.isEmpty()) {
                status += "|minutosOffnet";
            }

            if (minutosOnnet == null || minutosOnnet.isEmpty()) {
                status += "|minutosOnnet";
            }
            if (totalTelImp == null || totalTelImp.isEmpty()) {
                status += "|totalTelImp";
            }
        }

        /*
         * if( ofertaFijo == 1 ){
         * if (tipoSuscripcion == null || tipoSuscripcion.isEmpty()) {
         * status += "|tipoSuscripcion";
         * }
         * if (ofertaSuplementaria == null || ofertaSuplementaria.isEmpty()) {
         * status += "|ofertaSuplementaria";
         * }
         * if (observacionGeneral == null || observacionGeneral.isEmpty()) {
         * status += "|observacionGeneral";
         * }
         * }
         */
        if (ofertaFijo == 1) {
            if (ofertas != null) {
                for (int i = 0; i < ofertas.length; i++) {
                    if (ofertas[i].getObservacionGeneral() == null || ofertas[i].getObservacionGeneral().isEmpty()) {
                        status += "|ofertas.observacionGeneral";
                    }
                    if (ofertas[i].getOfertaSuplementaria() == null || ofertas[i].getOfertaSuplementaria().isEmpty()) {
                        status += "|ofertas.ofertaSuplementaria";
                    }
                    if (ofertas[i].getTipoSuscripcion() == null || ofertas[i].getTipoSuscripcion().isEmpty()) {
                        status += "|ofertas.tipoSuscripcion";
                    }
                    if (ofertas[i].getFechaEjecucion() == null || ofertas[i].getFechaEjecucion().isEmpty()) {
                        status += "|ofertas.fechaEjecucion";
                    }
                }
            } else {
                status += "|ofertas";
            }

        }

        if (recursosFijo == 1) {
            if (recursos != null) {
                for (int i = 0; i < recursos.length; i++) {
                    if (recursos[i].getObservacionGeneral() == null || recursos[i].getObservacionGeneral().isEmpty()) {
                        status += "|recursos.observacionGeneral";
                    }
                    if (recursos[i].getOfertaSuplementaria() == null
                            || recursos[i].getOfertaSuplementaria().isEmpty()) {
                        status += "|recursos.ofertaSuplementaria";
                    }
                    if (recursos[i].getTipoSuscripcion() == null || recursos[i].getTipoSuscripcion().isEmpty()) {
                        status += "|recursos.tipoSuscripcion";
                    }
                    if (recursos[i].getFechaEjecucion() == null || recursos[i].getFechaEjecucion().isEmpty()) {
                        status += "|recursos.fechaEjecucion";
                    }
                }
            } else {
                status += "|recursos";
            }
        }

        if (ipTv == 1) {
            if (servicioIpTv != null) {

                if (servicioIpTv.getPlan() == null || servicioIpTv.getPlan().isEmpty()) {
                    status += "|ServicioIpTv.plan";
                }
                if (servicioIpTv.getCantidad() == null || servicioIpTv.getCantidad().isEmpty()) {
                    status += "|ServicioIpTv.cantidad";
                }
                if (servicioIpTv.getBeforePrice() == null || servicioIpTv.getBeforePrice().isEmpty()) {
                    status += "|ServicioIpTv.beforePrice";
                }
                if (servicioIpTv.getAfterPrice() == null || servicioIpTv.getAfterPrice().isEmpty()) {
                    status += "|ServicioIpTv.afterPrice";
                }
            } else {
                status += "|ServicioIpTv";
            }
            if (adicionalIpTv != null) {
                for (int i = 0; i < adicionalIpTv.length; i++) {
                    if (adicionalIpTv[i].getPlan() == null || adicionalIpTv[i].getPlan().isEmpty()) {
                        status += "|adicionalIpTv.plan";
                    }
                    if (adicionalIpTv[i].getCantidad() == null || adicionalIpTv[i].getCantidad().isEmpty()) {
                        status += "|adicionalIpTv.cantidad";
                    }
                    if (adicionalIpTv[i].getBeforePrice() == null || adicionalIpTv[i].getBeforePrice().isEmpty()) {
                        status += "|adicionalIpTv.beforePrice";
                    }
                    if (adicionalIpTv[i].getAfterPrice() == null || adicionalIpTv[i].getAfterPrice().isEmpty()) {
                        status += "|adicionalIpTv.afterPrice";
                    }
                    /*
                     * if(String.valueOf(adicionalIpTv[i].getRecurringFlag())==null||String.valueOf(
                     * `
                     * adicionalIpTv[i].getRecurringFlag()).isEmpty()){
                     * status += "|adicionalIpTv.recurringFlag";
                     * }
                     */
                }
            }

            if (pagoInstalacionIptv != null) {
                // ||pagoInstalacionIptv.getDetalle().isEmpty()
                if (pagoInstalacionIptv.getDetalle() == null) {
                    status += "|pagoInstalacion.detalle";
                } else {
                    if (pagoInstalacionIptv.getDetalle().isEmpty()) {
                        pagoInstalacionIptv.setDetalle("Precio de instalacion IPTV");
                    }
                }
                // ||pagoInstalacionIptv.getCantidad().isEmpty()
                if (pagoInstalacionIptv.getCantidad() == null) {
                    status += "|pagoInstalacion.cantidad";
                } else {
                    if (pagoInstalacionIptv.getCantidad().isEmpty()) {
                        pagoInstalacionIptv.setCantidad("00.00");
                    }
                }
                // || pagoInstalacionIptv.getBeforePrice().isEmpty()
                if (pagoInstalacionIptv.getBeforePrice() == null) {
                    status += "|pagoInstalacion.beforePrice";
                } else {
                    if (pagoInstalacionIptv.getBeforePrice().isEmpty()) {
                        pagoInstalacionIptv.setBeforePrice("00.00");
                    }
                }
                // pagoInstalacionIptv.getAfterPrice().isEmpty()
                if (pagoInstalacionIptv.getAfterPrice() == null) {
                    status += "|pagoInstalacion.afterPrice";
                } else {
                    if (pagoInstalacionIptv.getAfterPrice().isEmpty()) {
                        pagoInstalacionIptv.setAfterPrice("00.00");
                    }
                }

            }

            if (modalidadPrestacion == null) {
                status += "|modalidadPrestacion";
            } else if (!(modalidadPrestacion.equals("F")
                    || modalidadPrestacion.equals("S")
                    || modalidadPrestacion.equals("O"))) {
                status += "|modalidadPrestacion debe ser F, S, O";
            }

            if (tipoClienteTV == null) {
                status += "|tipoClienteTV";
            } else if (!(tipoClienteTV.equals("R")
                    || tipoClienteTV.equals("P")
                    || tipoClienteTV.equals("C"))) {
                status += "|tipoClienteTV debe ser R, P, C";
            }

            // totalIptv
            if (totalIptv == null || totalIptv.isEmpty()) {
                status += "totalIptv";
            }
        }

        if (ipTv == 1) {
            if (iptvTirilla != null) {
                if (iptvTirilla.getTipoSuscripcion() == null || iptvTirilla.getTipoSuscripcion().isEmpty()) {
                    status += "|iptvTirilla.tipoSuscripcion";
                }

                if (iptvTirilla.getNoSuscripcion() == null || iptvTirilla.getNoSuscripcion().isEmpty()) {
                    status += "|iptvTirilla.noSuscripcion";
                }

                if (iptvTirilla.getObservacionAsesor() == null || iptvTirilla.getObservacionAsesor().isEmpty()) {
                    status += "|iptvTirilla.observacionAsesor";
                }

                if (iptvTirilla.getOfertaNueva() == null || iptvTirilla.getOfertaNueva().isEmpty()) {
                    status += "|iptvTirilla.ofertaNueva";
                }

                if (iptvTirilla.getCorreoElectronico() == null || iptvTirilla.getCorreoElectronico().isEmpty()) {
                    status += "|iptvTirilla.correoElectronico";
                }
            } else {
                status += "|iptvTirilla";
            }
        }

        if (ott == 1) {
            if (ottTirilla != null) {
                if (ottTirilla.getTipoSuscripcion() == null || ottTirilla.getTipoSuscripcion().isEmpty()) {
                    status += "|ottTirilla.tipoSuscripcion";
                }

                if (ottTirilla.getNoSuscripcion() == null || ottTirilla.getNoSuscripcion().isEmpty()) {
                    status += "|ottTirilla.noSuscripcion";
                }

                if (ottTirilla.getObservacionAsesor() == null || ottTirilla.getObservacionAsesor().isEmpty()) {
                    status += "|ottTirilla.observacionAsesor";
                }

                if (ottTirilla.getOfertaNueva() == null || ottTirilla.getOfertaNueva().isEmpty()) {
                    status += "|ottTirilla.ofertaNueva";
                }

                if (ottTirilla.getCorreoElectronico() == null || ottTirilla.getCorreoElectronico().isEmpty()) {
                    status += "|ottTirilla.correoElectronico";
                }
            } else {
                status += "|ottTirilla";
            }

            // totalOtt
            if (totalOtt == null || totalOtt.isEmpty()) {
                status += "totalOtt";
            }

        }

        if (ipTv == 1 || ott == 1) {

            if (hdTvRentaEquiposPr == null || hdTvRentaEquiposPr.isEmpty()) {
                status += "|hdTvRentaEquiposPr";
            }

            if (hdPrecioRentaEquipo == null || hdPrecioRentaEquipo.isEmpty()) {
                status += "|hdPrecioRentaEquipo";
            }
            if (sdTvRentaEquiposAd == null || sdTvRentaEquiposAd.isEmpty()) {
                status += "|sdTvRentaEquiposAd";
            }

            if (sdPrecioRentaEquipo == null || sdPrecioRentaEquipo.isEmpty()) {
                status += "|sdPrecioRentaEquipo";
            }
        }

        if (fmc == 1) {

            if (paqueteFmc == null || paqueteFmc.isEmpty()) {
                status += "|paqueteFmc";
            }

            if (detalleFmc == null || detalleFmc.length == 0) {
                status += "|servicioFmc";
            }

            if (Float.isNaN(priceBeforeFmc) || Float.toString(priceBeforeFmc).isEmpty()) {
                status += "|priceBeforeFmc";
            }

            if (Float.isNaN(priceAfterFmc) || Float.toString(priceAfterFmc).isEmpty()) {
                status += "|priceAfterFmc";
            }

            if (tamanioFmc == null || tamanioFmc.isEmpty()) {
                status += "|tamanioFmc";
            }

        }

        if (!(financiamiento == null || financiamiento.equals(""))) {
            if (tablaFinanciamientoSGA != null) {
                for (int i = 0; i < tablaFinanciamientoSGA.length; i++) {
                    if (tablaFinanciamientoSGA[i].getFechaFact() == null ||
                            tablaFinanciamientoSGA[i].getFechaFact().isEmpty()) {
                        status += "|tablaFinanciamientoSGA.fechaFact";
                    }
                    if (tablaFinanciamientoSGA[i].getMarcaEquipo() == null ||
                            tablaFinanciamientoSGA[i].getMarcaEquipo().isEmpty()) {
                        status += "|tablaFinanciamientoSGA.farcaEquipo";
                    }
                    if (tablaFinanciamientoSGA[i].getNumFactura() == null ||
                            tablaFinanciamientoSGA[i].getNumFactura().isEmpty()) {
                        status += "|tablaFinanciamientoSGA.fumFactura";
                    }
                    if (tablaFinanciamientoSGA[i].getSriFact() == null ||
                            tablaFinanciamientoSGA[i].getSriFact().isEmpty()) {
                        status += "|tablaFinanciamientoSGA.sriFact";
                    }
                    if (tablaFinanciamientoSGA[i].getUsuarioFact() == null ||
                            tablaFinanciamientoSGA[i].getUsuarioFact().isEmpty()) {
                        status += "|tablaFinanciamientoSGA.usuarioFactura";
                    }
                    if (tablaFinanciamientoSGA[i].getValor() == null ||
                            tablaFinanciamientoSGA[i].getValor().isEmpty()) {
                        status += "|tablaFinanciamientoSGA.valor";
                    }
                }
            } else {
                status += "|tablaFinanciamientoSGA";
            }

            if (tablaCuotasFinanciamiento != null) {
                for (int i = 0; i < tablaCuotasFinanciamiento.length; i++) {
                    if (tablaCuotasFinanciamiento[i].getConcepto() == null ||
                            tablaCuotasFinanciamiento[i].getConcepto().isEmpty()) {
                        status += "|tablaCuotasFinanciamiento.concepto";
                    }
                    if (tablaCuotasFinanciamiento[i].getFechaPago() == null ||
                            tablaCuotasFinanciamiento[i].getFechaPago().isEmpty()) {
                        status += "|tablaCuotasFinanciamiento.fechaPago";
                    }
                    if (tablaCuotasFinanciamiento[i].getCuota() == null ||
                            tablaCuotasFinanciamiento[i].getCuota().isEmpty()) {
                        status += "|tablaCuotasFinanciamiento.cuota";
                    }
                }
            } else {
                status += "|tablaCuotasFinanciamiento";
            }

            if (tablaPagareResource != null) {
                for (int i = 0; i < tablaPagareResource.length; i++) {
                    int cuota = Integer.parseInt(tablaPagareResource[i].getCuota());

                    if (cuota < 1) {
                        status += "|tablaPagareResource.cuota";
                    }
                    if (tablaPagareResource[i].getFechaPago() == null ||
                            tablaPagareResource[i].getFechaPago().isEmpty()) {
                        status += "|tablaPagareResource.pago";
                    }
                    if (tablaPagareResource[i].getCuotaLetra() == null ||
                            tablaPagareResource[i].getCuotaLetra().isEmpty()) {
                        status += "|tablaPagareResource.cuotaLetra";
                    }
                }
            } else {
                status += "|tablaPagareResource";
            }
        }

        if (usuarioDigitador == null || usuarioDigitador.isEmpty()) {
            status += "|usuarioDigitador";
        }

        if (oficinaNombre == null || oficinaNombre.isEmpty()) {
            status += "|oficinaNombre";
        }

        if (clienteNombres == null || clienteNombres.isEmpty()) {
            status += "|clienteNombres";
        }

        if (personaEmail == null || personaEmail.isEmpty()) {
            status += "|personaEmail";
        }

        if (clientePrimerApellido == null || clientePrimerApellido.isEmpty()) {
            status += "|clientePrimerApellido";
        }

        if (clienteSegundoApellido == null || clienteSegundoApellido.isEmpty()) {
            clienteSegundoApellido = "";
        }

        if (clienteCedula == null || clienteCedula.isEmpty()) {
            status += "|clienteCedula";
        }

        if (clienteServicio == null || clienteServicio.isEmpty()) {
            status += "|clienteServicio";
        }

        if (!status.isEmpty() && status.charAt(0) == '|') {
            status = status.substring(1);
        }

        return status;
    }

    public String formaPago(String tipoPago) {
        String result = "";
        switch (tipoPago) {

            case "T":
                result = "Cargo Tarjeta de Cr&eacute;dito:";
                break;
            case "C":
                result = "D&eacute;bito Cta. Cte:";
                break;
            case "A":
                result = "D&eacute;bito Cta. Ahorros:";
                break;
            case "F":
                result = "Contra factura:";
                break;
            case "":
                result = "";
                break;
            default:
                result = "";
        }
        return result;
    }

    public String tipoCliente(String tipoCliente) {

        String result = "";

        switch (tipoCliente) {

            case "R":
                result = "Residencial";
                break;
            case "P":
                result = "Pymes";
                break;
            case "C":
                result = "Corporativo";
                break;
            default:
                result = "";
        }

        return result;
    }
}