
package net.sasf.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Cacheable(false)
@NoArgsConstructor
@Table(name="CS_PLANTILLA")
@NamedQuery(
    name="CSPlantilla.findAll", 
    query="SELECT h FROM CSPlantilla h"
)
public class CSPlantilla extends PanacheEntityBase {

    @Id
    @Column(name = "id_plantilla")
    private Long idPlantilla;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "tipo")
    private Long tipo;
    
    @Column(name = "contenido")
    private String contenido;
    
}
