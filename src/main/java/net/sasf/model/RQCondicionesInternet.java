package net.sasf.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RQCondicionesInternet {
   private String servicio;
    private String compartido;
    private String comparticion;
    private String velocidad;
    private String velocidadMinima;
    private String disponibilidad;

}
