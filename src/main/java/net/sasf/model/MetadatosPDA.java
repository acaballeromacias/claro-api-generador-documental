package net.sasf.model;


public class MetadatosPDA {

    public static final String MD_TIPO_PERSONA = "%%PERSONA:TIPOCF%%";
    public static final String MD_CLIENTE_CEDULA = "%%CLIENTE:CEDULA%%";
    public static final String MD_OFICINA_CIUDAD = "%%OFICINA:CIUDAD%%";
    public static final String MD_TRAMITE_ID = "%%TRAMITE:ID%%";
    public static final String MD_TRAMITE_DIA = "%%TRAMITE:DIA%%";
    public static final String MD_TRAMITE_MES = "%%TRAMITE:MES%%";
    public static final String MD_TRAMITE_ANIO = "%%TRAMITE:ANIO%%";
    public static final String MD_CLIENTE_FIRMA = "%%CLIENTE:FIRMA%%";
    public static final String MD_DOM_PROVINCIA = "%%PROVINCIA%%";
    public static final String MD_DOM_CIUDAD = "%%DOM:CIUDAD%%";
    public static final String MD_TRECERA_EDAD = "%%TERCERA:EDAD%%";
    public static final String MD_CLIEN_DISCA = "%%CLIEN:DISCA%%";
    public static final String MD_CLIENTE_PRIMER_APELLIDO = "%%CLIENTE:PRIMER:APELLIDO%%";
    public static final String MD_CLIENTE_SEGUNDO_APELLIDO = "%%CLIENTE:SEGUNDO:APELLIDO%%";
    public static final String MD_CLIENTE_NOMBRES = "%%CLIENTE:NOMBRES%%";
    public static final String MD_CLIENTE_NOMBRE_COMPLETO = "%%CLIENTE:NOMBRE:COMPLETO%%";
    public static final String MD_CLIENTE_IDENTIFICACION = "%%CLIENTE:IDENT%%";
    public static final String MD_FECHA_ACTUAL = "%%FECHA:ACTUAL%%";
    public static final String MD_FECHA_TRAMITE = "%%FECHA:TRAMITE%%";
    public static final String MD_DIRRECCION_INSTAL = "%%DIRRECCION:INSTAL%%";

    public static final String MD_CLIENTE_NACIONALIDAD = "%%CLIENTE:NACIONALIDAD%%";
    public static final String MD_DOM_PROVINCIA1 = "%%DOM:PRONVICIA%%";
    public static final String MD_DOM_CANTON = "%%DOM:CANTON%%";
    public static final String MD_DOM_PARROQUIA = "%%DOM:PARROQUIA%%";
    public static final String MD_INSTAL_PARROQUIA = "%%INSTAL:PARROQUIA%%";
    public static final String MD_DOM_CALLE_P = "%%DOM:CALLE:P%%";
    public static final String MD_DOM_CALLE_S = "%%DOM:CALLE:S%%";
    public static final String MD_DOM_NUMERACION = "%%DOM:NUERACION%%";
    public static final String MD_DOM_EDIFICIO = "%%DOM:EDIFICIO%%";
    public static final String MD_DOM_PISO = "%%DOM:PISO%%";
    public static final String MD_DOM_OFICINA = "%%DOM:OFOCINA%%";
    public static final String MD_DOM_REFERENCIA = "%%DOM:REFERENCIA%%";
    public static final String MD_PRICE_BEFORE_INT = "%%PRICE:BEFORE:INT%%";
    public static final String MD_PRICE_AFTER_INT = "%%PRICE:AFTER:INT%%";
    public static final String MD_TIPO_CLIENTE = "%%TIPO:CLIENTE%%";
    public static final String MD_CLIENTE_FECHA_NACIMIENTO = "%%CLIENTE:FECHA:NACIMIENTO%%";
    public static final String MD_DOM_DIR_COMP = "%%DOM:DIR:COMP%%";
    public static final String MD_DOM_TELEF = "%%DOM:TELEF%%";
    public static final String MD_DOM_TELEF1 = "%%DOM:TELEF1%%";
    public static final String MD_VIVIENDA_ENTRADA = "%%VIVIENDA:ENTRADA%%";
    public static final String MD_PERSONA_EMAIL = "%%PERSONA:EMAIL%%";
    public static final String MD_CORREO_PERSONA = "%%CORREO:PER%%";
    public static final String MD_DIRECC_ORDEN = "%%DIRECC:ORDEN%%";
    public static final String MD_PLAN_ACT_INT = "%%PLAN:ACT:INT%%";
    public static final String MD_BEFORE_SUSC_INT = "%%BEFORE:SUSC:INT%%";
    public static final String MD_AFTER_SUSC_INT = "%%AFTER:SUSC:INT%%";
    public static final String MD_PERMIN_CLI_INT = "%%PERMIN:CLI:INT%%";
    public static final String MD_PERMIN_TIEMPO_CLI_INT = "%%PERMIN:TIEMPO:CLI:INT%%";
    public static final String MD_PERMIN_BENEF_CLI_INT = "%%PERMIN:BENEF:CLI:INT%%";
    public static final String MD_VELOCIDAD_MIN = "%%VELOCIDAD:MIN%%";
    public static final String MD_VELOCIDAD_MAX = "%%VELOCIDAD:MAX%%";
    public static final String MD_ADICIONAL_INTERNET = "%%ADICIONAL:INTERNET%%";
    public static final String MD_MENS_ADICIONAL_INT = "%%MENS:ADICIONAL:INT%%";
    public static final String MD_SUBTOTAL_INT = "%%SUBTOTAL:INT%%";
    public static final String MD_IVA_INT = "%%IVA:INT%%";
    public static final String MD_TOTAL_INT = "%%TOTAL:INT%%";
    public static final String MD_CONDI_INTERNET = "%%CONDI:INTERNET%%";
    public static final String MD_FORMA_PAGO_1 = "%%FORMA:PAGO:1%%";
    public static final String MD_PAGO_FORMA_FIJO = "%%PAGO:FORMA:FIJO%%";
    public static final String MD_TARJETA_NOMBRE_CLIENTE = "%%TARJETA:NOMBRE:CLIENTE%%";
    public static final String MD_NOMBRE_TITULAR_BAN = "%%NOMBRE:TITULAR:BAN%%";
    public static final String MD_DIRRECCION_INSTALACION = "%%DIRRECCION:INSTALACION%%";
    public static final String MD_DOMICILIO_EMAIL = "%%DOM:EMAIL%%";
    public static final String MD_SOLICITUD_ID = "%%SOLICITUD:ID%%";
    public static final String MD_CODIGO_VENDEDOR = "%%CODIGO:VENDEDOR%%";
    public static final String MD_TIPO_SUSCRIP = "%%TIPO:SUSCRIP%%";
    public static final String MD_OFERTAS_ACTIVAN = "%%ACT:FEAT:SUPL%%";
    public static final String MD_COMENTARIO_ASESOR = "%%OBS:GRAL%%";
    public static final String MD_PRODUCTO_INTERNET = "%%PRODUCTO:INTERNET%%";
    public static final String MD_FUTURE_DATE = "%%FUTURE:DATE%%";

    public static final String MD_OFF_NET = "%%OFF:NET%%";
    public static final String MD_ON_NET = "%%ON:NET%%";
    public static final String MD_PLAN_ACT_TEL = "%%PLAN:ACT:TEL%%";
    public static final String MD_PRICE_BEFORE_TEL = "%%PRICE:BEFORE:TEL%%";
    public static final String MD_PRICE_AFTER_TEL = "%%PRICE:AFTER:TEL%%";
    public static final String MD_ADICIONAL_TELEFONIA = "%%ADICIONAL:TELEFONIA%%";
    public static final String MD_BEFORE_SUSC_TEL = "%%BEFORE:SUSC:TEL%%";
    public static final String MD_AFTER_SUSC_TEL = "%%AFTER:SUSC:TEL%%";
    public static final String MD_MENS_ADICIONAL_TEL = "%%MENS:ADICIONAL:TEL%%";
    public static final String MD_SUBTOTAL_TEL = "%%SUBTOTAL:TEL%%";
    public static final String MD_IVA_TEL = "%%IVA:TEL%%";
    public static final String MD_TOTAL_TEL = "%%TOTAL:TEL%%";
    public static final String MD_PERMIN_CLI_TEL = "%%PERMIN:CLI:TEL%%";
    public static final String MD_PERMIN_TIEMPO_CLI_TEL = "%%PERMIN:TIEMPO:CLI:TEL%%";
    public static final String MD_PERMIN_BENEF_CLI_TEL = "%%PERMIN:BENEF:CLI:TEL%%";
    public static final String MD_GENERAL_INSTAL = "%%GENERAL:INSTAL%%";
    public static final String MD_GENERAL_DESC = "%%GENERAL:DESC%%";
    public static final String MD_BANCO_NOMBRE_COMPLETO = "%%BAN:NOM:COMPLETO%%";
    public static final String MD_TARJETA_FORMA_PAGODEB = "%%TARJETA:FORMA:PAGODEB%%";
    public static final String MD_BANCO_CUENTA_DEB = "%%BAN:CUENTA:DEB%%";
    public static final String MD_TARJETA_NUMERO = "%%TARJETA:NUMERO%%";
    public static final String MD_BANCO_NUMERO_CUENTA_BANCARIA = "%%BANCO:NUMERO:CUENTA:BANCARIA%%";
    public static final String MD_CLIENTE_TIPOIDEN = "%%CLIENTE:TIPOIDEN%%";
    
    public static final String MD_PLAN_ACT_TV = "%%PLAN:ACT:TV%%";
    public static final String MD_PRICE_BEFORE_TV = "%%PRICE:BEFORE:TV%%";
    public static final String MD_PRICE_AFTER_TV = "%%PRICE:AFTER:TV%%";
    public static final String MD_ADICIONAL_TELEVISION = "%%ADICIONAL:TELEVISION%%";
    public static final String MD_TOTAL_TV = "%%TOTAL:TV%%";
    public static final String MD_BEFORE_SUSC_TV = "%%BEFORE:SUSC:TV%%";
    public static final String MD_AFTER_SUSC_TV = "%%AFTER:SUSC:TV%%";
    public static final String MD_PERMIN_CLI_TV = "%%PERMIN:CLI:TV%%";
    public static final String MD_PERMIN_TIEMPO_CLI_TV = "%%PERMIN:TIEMPO:CLI:TV%%";
    public static final String MD_PERMIN_BENEF_CLI_TV = "%%PERMIN:BENEF:CLI:TV%%";
    public static final String MD_CANALES_AUDIO_TV = "%%CANALES:AUDIO:TV%%";
    public static final String MD_CANALES_VIDEO_TV = "%%CANALES:VIDEO:TV%%";
    public static final String MD_PAGO_UNICO_FIJO = "%%PAGO:UNICO:FIJO%%";
    
    public static final String MD_TRAMITE_NUMERO = "%%TRAMITE:NUMERO%%";
    public static final String MD_INTERNET_FIJO = "%%INT:FIJO%%";
    public static final String MD_TELEFONIA_FIJO = "%%TEL:FIJO%%";
    public static final String MD_TELEVISION_FIJO = "%%TV:FIJO%%";
    public static final String MD_TOTAL_INSTALACION = "%%TOTAL:INSTALACION%%";

    public static final String MD_CLIENTE_SERVICIO = "%%CLIENTE:SERVICIO%%";
    public static final String MD_SERVICE_TEL = "%%SERVICE:TEL%%";
    public static final String MD_ID_SERVICIO = "%%ID:SERVICIO%%";
    public static final String MD_NUMERO_CUENTA_FACTURACION = "%%NUM:CUENTA:FACT%%";
    
    public static final String MD_USUARIO_DIGITADOR = "%%USUARIO:DIGITADOR%%";
    public static final String MD_NOMBRE_USUARIO = "%%NOMBRE:USUARIO%%";
    public static final String MD_ID_OFIC = "%%ID:OFIC%%";
    public static final String MD_OFICINA_NOMBRE = "%%OFICINA:NOMBRE%%";
    public static final String MD_FINANCIAMIENTO = "%%FINAN:ID%%";
    public static final String MD_NUM_CUENTA_BSCS = "%%NUM:CUENTA:BSCS%%";
    
    public static final String MD_TABLA_DET_FIN_SGA = "%%TABLA:DET:FIN:SGA%%";    
    public static final String MD_TABLA_DET_FIN_FIJO = "%%CAB:FINAN%%";
    
    public static final String MD_BASE_FINANCIAMIENTO = "%%VALOR:BASE:FINANCIAMIENTO%%";
    public static final String MD_VALOR_IVA_FINANCIAMIENTO = "%%VALOR:IVA:FINANCIAMIENTO%%";
    public static final String MD_SUMA_VALORES = "%%SUMA:VALORES%%";
    public static final String MD_CUOTA_INICIAL = "%%CUOTA:INICIAL%%";
    
    public static final String MD_TABLA_CUOTAS_FIN = "%%TABLA:CUOTAS:FIN%%";
    
    public static final String MD_TOTAL_LETRAS = "%%TOTAL:LETRAS%%";
    
    public static final String MD_TABLA_PAGARE_RESOURCE = "%%TABLA:PAGARE:RESOURCE2%%";
    
    public static final String MD_TABLA_PAGARE_FIJO = "%%TABLA:PAGARE:FIJO%%";
    
    public static final String MD_ID_FINAN = "%%FINANCIAMIENTO%%";
    public static final String MD_CAB_FINAN = "%%CAB:FINAN%%";
    public static final String MD_FINAN_FIJO = "%%FINAN:FIJO%%";
    
    
    public static final String MD_PROVINCIA_INSTALACION_DTH = "%%PROVINCIA:INSTALACION:DTH%%";
    public static final String MD_CIUDAD_INSTALACION_DTH = "%%CIUDAD:INSTALACION:DTH%%";
    public static final String MD_DIRECCION_INSTALACION_DTH = "%%DIRECCION:INSTALACION:DTH%%";
    public static final String MD_MOD_FISICO = "%%MOD:FISICO%%";
    public static final String MD_MOD_SATELITAL = "%%MOD:SATELITE%%";
    public static final String MD_TABLA_DESCRIPCION_SERV_DTH = "%%TABLA:DESCRIPCION:SERV:DTH%%";
    
    public static final String MD_TABLA_PAQ_ADIC_DTH = "%%TABLA:PAQ:ADIC:DTH%%";
    public static final String MD_TABLA_INSTAL_DTH = "%%TABLA:INSTAL;DTH%%";
    
    public static final String MD_PERMIN_TIEMPO_CLI= "%%PERMIN:TIEMPO:CLI%%";
    public static final String MD_PERMIN_CLI= "%%PERMIN:CLI%%";
    public static final String MD_PERMIN_BENEF_CLI= "%%PERMIN:BENEF:CLI%%";
    public static final String MD_TIPO_SUSC = "%%TIPO:SUSC%%";
    public static final String MD_CANALES_AUDIO = "%%CANALES:AUDIO%%";
    public static final String MD_CANALES_VIDEO = "%%CANALES:VIDEO%%";
    public static final String MD_TOTAL_DTH_IMPUESTO = "%%TOTAL:DTH:IMPUESTOS%%";
    public static final String MD_BAN_NOMB_CLI = "%%BAN:NOMB:CLI%%";
    
    //DECO
    public static final String MD_TABLA_DECO_PRINCIPAL = "%%DECO:PRINCIPAL%%";
    public static final String MD_TABLA_DECO_ADICIOALES = "%%DECO:ADICIONALES%%";
      
    public static final String MD_LOGO_CLARO = "%%LOGO:CLARO%%";
    public static final String MD_LOGO_CARTILLA = "%%LOGO:CARTILLA%%";
    
    // ------------------------------------------------------------- FMC 
    public static final String MD_FMC_PROGRAMADA     = "%%FMC.PROGAMADA%%";    
    public static final String MD_FMC_GROUP          = "%%FMC.GROUP%%";
    public static final String MD_DESACT_FEA_SUPL    = "%%DESACT:FEAT:SUPL%%";
    public static final String MD_DESACT_PLAN_ACT    = "%%DESC:PLAN:ACT%%";
    public static final String MD_DESACT_PLAN_ANT    = "%%DESC:PLAN:ANT%%";
    public static final String MD_STANDAR_ADDRESS_ID = "%%STANDAR:ADDRESS:ID%%";
    public static final String MD_DIR_ORIGEN         = "%%DIR:ORIGEN%%";
    
    public static final String MD_FORMA_PAGO_TEXTO   = "%%FORMA:PAGO:TEXTO%%";
    public static final String MD_PAQUETE_FMC        = "%%PAQUETE:FMC%%";
    public static final String MD_DETALLE_FMC        = "%%DETALLE:FMC%%";
    public static final String MD_PRICE_BEFORE_FMC   = "%%PRICE:BEFORE:FMC%%";
    public static final String MD_PRICE_AFTER_FMC    = "%%PRICE:AFTER:FMC%%";
    public static final String MD_TAMANIO_FMC        = "%%TAMANIO:FMC%%";
    public static final String MD_CABECERA_FMC       = "%%CABECERA:FMC%%";
    // ------------------------------------------------------------- 
    //NUEVO
    public static final String MD_OTT_FIJO = "%%OTT:FIJO%%";
    public static final String MD_BEFORE_SUSC_OTT = "%%BEFORE:SUSC:OTT%%";
    public static final String MD_AFTER_SUSC_OTT = "%%AFTER:SUSC:OTT%%";
    
    public static final String MD_TABLA_DET_FIN_V_ONE = "%%TABLA:DET:FIN:V:ONE%%";
    
    public static final String MD_PLAN_ACT_OTT = "%%PLAN:ACT:OTT%%";
    public static final String MD_TOTAL_OTT = "%%TOTAL:OTT%%";
    public static final String MD_SERVICE_OTT = "%%SERVICE:OTT%%";
    public static final String MD_TOTAL_IPTV = "%%TOTAL:IPTV%%";
    public static final String MD_TIPO_CLIENTE_TV = "%%TIPO:CLIENTE:TV%%";
    public static final String MD_PRICE_BEFORE_IPTV = "%%PRICE:BEFORE:IPTV%%";
    public static final String MD_PRICE_AFTER_IPTV    = "%%PRICE:AFTER:IPTV%%";
    public static final String MD_PLAN_ACT_IPTV = "%%PLAN:ACT:IPTV%%";
    public static final String MD_PERMIN_TIEMPO_CLI_IPTV= "%%PERMIN:TIEMPO:CLI:IPTV%%";
    public static final String MD_PERMIN_CLI_IPTV= "%%PERMIN:CLI:IPTV%%";
    public static final String MD_PERMIN_BENEF_CLI_IPTV= "%%PERMIN:BENEF:CLI:IPTV%%";
    public static final String MD_CANALES_VIDEO_IPTV = "%%CANALES:VIDEO:IPTV%%";
    public static final String MD_CANALES_AUDIO_IPTV = "%%CANALES:AUDIO:IPTV%%";
    public static final String MD_BEFORE_SUSC_IPTV = "%%BEFORE:SUSC:IPTV%%";
    public static final String MD_AFTER_SUSC_IPTV = "%%AFTER:SUSC:IPTV%%";
    public static final String MD_ADICIONAL_IPTV = "%%ADICIONAL:IPTV%%";
    
    // Deposito de Garantia 
    public static final String MD_VALOR_DEPOSITO_GARANTIA_INT = "%%VALOR:DEPOSITO%%";
    public static final String MD_DEPOSITO_GARANTIA_INT = "%%DEPOSITO:GARANTIA%%";
    
    
    
    // Identificadores para los archivos de fotos
    public static final int IMG_CEDULA_ANVERSO = 901001;
    public static final int IMG_CEDULA_REVERSO = 901002;
    public static final int IMG_FOTO_LUGAR = 901003;
    public static final int IMG_PLANILLA_SB = 901004;
    public static final int IMG_FACTURA_OTROS = 901005;

    public MetadatosPDA() {
        super();
    }

    
    public static String nombreMes(int mes) {

        String[] meses = {
            "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre",
            "noviembre", "diciembre"
        };

        if(mes < 1 || mes > 12){
            mes=0;
        }

        return meses[mes - 1];
    }
    
    public static String radioButton(String valor, String valida, String atributo){
        String span ="";
        String imagRadio = "";
        String etiqueta = "";
        
        if (valor.equals(valida)) {
            span = "<span>&nbsp;" + atributo + "&nbsp;</span>";
            imagRadio = "<span class=\"circulo\">X</span>";
        } else {
            span = "<span>&nbsp;" + atributo + "&nbsp;</span>";
            imagRadio = "<span class=\"circulo\" style=\"color:#ffffff;\">X</span>";
        }
        etiqueta = span+imagRadio;
        return etiqueta;
    }

    public static String etiquetaImagen(String origen, int ancho) {

        String result = "<img src=\"-o-\" style=\"width:-a-px\" />";

        return result.replace("-o-", origen).replace("-a-", String.valueOf(ancho));
    }

    public static final String[] APROBADOS_DOC1y2 = { "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC3 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC4 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC5 = { "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC6 = { "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC14 = { "LOGO:CLARO" };
    public static final String[] APROBADOS_DOC15 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC12 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC11 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC16 = { "LOGO:CARTILLA", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC17 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
    public static final String[] APROBADOS_DOC18 = { "LOGO:CLARO", "CLIENTE:FIRMA" };
}
