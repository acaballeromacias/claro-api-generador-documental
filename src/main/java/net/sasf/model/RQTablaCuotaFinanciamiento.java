
package net.sasf.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RQTablaCuotaFinanciamiento {
    private String concepto;
    private String cuota;
    private String fechaPago;
}
