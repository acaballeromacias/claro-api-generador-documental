package net.sasf.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RQOfertas {
    private String ofertaSuplementaria;
    private String observacionGeneral;
    private String tipoSuscripcion;
    private String fechaEjecucion;
}
