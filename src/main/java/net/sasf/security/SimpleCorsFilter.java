package net.sasf.security;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import io.vertx.core.http.HttpServerRequest;

@Provider
public class SimpleCorsFilter implements ContainerRequestFilter {

    private static final Logger LOG = Logger.getLogger(SimpleCorsFilter.class);

    @Context
    UriInfo info;

    @Context
    HttpServerRequest request;

    @Override
    public void filter(ContainerRequestContext ctxReq) {

        var method = ctxReq.getMethod();
        var path = info.getPath();
        var address = request.remoteAddress().toString();

        ctxReq.getHeaders().add("Access-Control-Allow-Origin", "*");
        ctxReq.getHeaders().addAll("Access-Control-Allow-Methods", "POST", "PUT", "GET", "OPTIONS", "DELETE");
        ctxReq.getHeaders().add("Access-Control-Allow-Headers", "*");
        ctxReq.getHeaders().addAll("Access-Control-Allow-Headers", "Origin", "Content-Type", "X-Auth-Token", "Accept",
                "Authorization", "X-Request-With");

        LOG.infof("Request %s %s from IP %s", method, path, address);
    }
}