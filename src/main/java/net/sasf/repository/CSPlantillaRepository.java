
package net.sasf.repository;

import javax.enterprise.context.ApplicationScoped;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import net.sasf.model.CSPlantilla;

@ApplicationScoped
public class CSPlantillaRepository 
    implements PanacheRepositoryBase<CSPlantilla, Long> {
    
}
