package net.sasf.repository;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import net.sasf.model.EventLog;

@ApplicationScoped
public class EventLogRepository implements PanacheRepository<EventLog>{ 
    
}
