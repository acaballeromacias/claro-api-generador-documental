
package net.sasf.Healt;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;

import net.sasf.repository.CSPlantillaRepository;

@Readiness
@ApplicationScoped
public class DatabaseHC implements HealthCheck {

    @Inject
    private CSPlantillaRepository plantillaRepository;

    @Override
    public HealthCheckResponse call() {

        HealthCheckResponse databaseCheckRes;

        try {
    
            this.plantillaRepository.count();
            databaseCheckRes = HealthCheckResponse.up("Base de datos funcional");
            
        } catch (Exception e) { 
            databaseCheckRes = HealthCheckResponse.down("Base de datos no funcional"); }

        return databaseCheckRes;

    }

}
