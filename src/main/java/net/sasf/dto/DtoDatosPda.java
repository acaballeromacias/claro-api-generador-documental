package net.sasf.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sasf.model.DatosPDA;
import net.sasf.model.MetadatosPDA;
import net.sasf.model.RQAdicionalInternet;
import net.sasf.model.RQAdicionalTelefonia;
import net.sasf.model.RQAdicionalTelevision;
import net.sasf.model.RQOfertas;
import net.sasf.model.RQTablaCuotaFinanciamiento;
import net.sasf.model.RQDetallesFinanciamiento;
import net.sasf.model.RQTablaPagareResource;
import net.sasf.model.ServicioAdicionalIptv;
import net.sasf.model.ServicioFmc;
import net.sasf.service.contract.DocumentGeneratorService;

@Setter
@Getter
@NoArgsConstructor
public class DtoDatosPda {
    // Datos normales
    private DatosPDA datosRequest;

    // Datos de lógica
    private String mesString;
    private String tipoTarjeta;
    private String fechaFormateada;
    private double sumaValores1Formateada;
    private String tablaPagareResource;
    private String radioButtonTerceraEdad;
    private String radioButtonClienteDiscap;
    private String adicionalTelevisor;
    private String tablaServiciosPagoUnicoTel;
    private String tieneInternetFijo;
    private String tieneTelefoniaFijo;
    private String tieneOttFijo;
    private String tieneTvFijo;
    private String afterSuscInternet;
    private String beforeSuscInternet;
    private String priceAfterTelefonia;
    private String afterSuscTelefonia;
    private String beforeSuscTelefonia;
    private String afterSuscTv;
    private String beforeSuscTv;
    private String totalInstalacion;
    private String totalInstalacionOtt;
    private String strCab;
    private String strDet;
    private String priceBeforeFmc;
    private String priceAfterFmc;
    private String tipoTarjetatipoBanco;
    private String tipoBancoTipoTarjeta;
    private String nombreBancarioCondicional;
    private String afterBeforeInternet;
    private String totalInternet;
    private String adicionalInternet;
    private String perminBenefCli;
    private String radioButtonDepositoGarantia;
    private String radioButtonPerminCli;
    private String radioButtonPagoFijo;
    private String planActualTelefoniaFormateada;
    private String telefonoCli;
    private String perminTiempoCli;
    private String beforeSuscTel;
    private String afterSuscTel;
    private String tablaCoutaFinanciamiento;
    private String tablaDetalleFinanciamiento;
    private String perminBenefCliTv;
    private String tablaDecoPrincipal;
    private String tablaDecoAdicional;
    private String nombreCompletoBanco;
    private String identificacionBancaria;
    private String beforeSuscOtt;
    private String afterSuscOtt;
    private String tablaPagareFijo;
    private String adicionalTelefonia;
    private String totalTelImp;
    private String radioButtonCliente;
    private String permiCliIptv;
    private String adicionalIpTv;
    private String logoClaro;
    private RQOfertas ofertasRecursos;

    public String getLogoClaro() {
        String directorioBase = new File("src/main/resources/").getAbsolutePath();
        String logoClaro = directorioBase.replace("\\", "/");
        String img = "<img src=\"file:///" + logoClaro + "/public/logoclaro.jpg\" height=\"80px\" width=\"80px\"/>";
        return img;
    }

    // Inyeccion
    @Inject
    private DocumentGeneratorService pdfGenerator;

    // adicionalIpTv
    public String getAdicionalIpTv() {
        return creaTablaServiciosAdicionalesIptv(datosRequest.getAdicionalIpTv(), 0, 0);
    }

    // permiCliIptv
    public String getPermiCliIptv() {
        String permiCliIptv = datosRequest.getPerminBenefCli()
                .replaceAll("Telefonia", "IPTV")
                .replaceAll("telefonia", "IPTV")
                .replaceAll("OTT", "IPTV")
                .replaceAll("Internet", "IPTV")
                .replaceAll("internet", "IPTV");

        return permiCliIptv;
    }

    // radioButtonCliente
    public String getRadioButtonCliente() {
        String[] cliente = { "R", "P", "C" };
        String radioButtonCliente = "";
        for (int i = 0; i < cliente.length; i++) {
            radioButtonCliente = radioButtonCliente +
                    MetadatosPDA.radioButton(datosRequest.getTipoClienteTV(), cliente[i],
                            datosRequest.tipoCliente(cliente[i]));
        }
        return radioButtonCliente;
    }

    // totalTelImp
    public String getTotalTelImp() {
        double totalTelImp = 0.00;
        try {
            double adicional = 0;
            double iva = iva();

            if (datosRequest.getAdicionalTelefonia() != null && datosRequest.getAdicionalTelefonia().length > 0) {
                for (int i = 0; i < datosRequest.getAdicionalTelefonia().length; i++) {
                    adicional += Double.parseDouble(datosRequest.getAdicionalTelefonia()[i].getBeforePrice());
                }
            }

            double subtotal = Double.parseDouble(datosRequest.getPriceBeforeTelefonia()) + adicional;

            iva = subtotal * iva / (iva > 1 ? 100 : 1);

            totalTelImp = subtotal + iva;
        } catch (Exception e) {
            totalTelImp = 0.00;
            throw new NoResultException("Error al calcular el totalTelImp");
        }
        return String.format("%.2f", totalTelImp);
    }

    // adicionalTelefonia
    public String getAdicionalTelefonia() {
        double adicional = 0;
        if (datosRequest.getAdicionalTelefonia() != null && datosRequest.getAdicionalTelefonia().length > 0) {
            for (int i = 0; i < datosRequest.getAdicionalTelefonia().length; i++) {
                adicional += Double.parseDouble(datosRequest.getAdicionalTelefonia()[i].getBeforePrice());
            }
        }
        return creaTablaServiciosAdicionalesTelefonia(datosRequest.getAdicionalTelefonia(), adicional);
    }

    // tablaPagareFijo
    public String getTablaPagareFijo() {
        return creaTablaPagareResource(datosRequest.getTablaPagareResource());
    }

    // beforeSuscOtt
    public String getBeforeSuscOtt() {
        String beforeSuscOtt = "";
        if (datosRequest.getOtt() == 1) {
            beforeSuscOtt = datosRequest.getPagoInstalacionOtt().getBeforePrice();
        } else {
            beforeSuscOtt = "0.00";
        }
        return beforeSuscOtt;
    }

    // afterSuscOtt
    public String getAfterSuscOtt() {
        Double afterSuscOtt = 0.00;
        try {

            if (datosRequest.getOtt() == 1) {
                double iva = iva();
                double beforeSusOtt = Double.parseDouble(getBeforeSuscOtt());
                afterSuscOtt = beforeSusOtt + (beforeSusOtt * iva / (iva > 1 ? 100 : 1));
            } else {
                afterSuscOtt = 0.00;
            }

        } catch (Exception e) {
            afterSuscOtt = 0.00;
            throw new NoResultException("Error al calcular el afterSuscOtt");
        }
        return String.format("%.2f", afterSuscOtt);
    }

    // identificacionBancaria
    public String identificacionBancaria() {
        String identificacionBan = "";
        if (datosRequest.getClienteIdent() == null || datosRequest.getClienteIdent().isEmpty()) {
            identificacionBan = datosRequest.getClienteCedula();
        } else {
            identificacionBan = datosRequest.getClienteIdent();
        }
        return identificacionBan;
    }

    // nombreCompletoBanco
    public String getNombreCompletoBanco() {
        String nombreBanco = "";
        if (datosRequest.getNombreTitularTarjeta() == null || datosRequest.getNombreTitularTarjeta().isEmpty()) {
            nombreBanco = datosRequest.getNombreTitularCta();
        } else {
            nombreBanco = datosRequest.getNombreTitularTarjeta();
        }
        return nombreBanco;
    }

    // afterSuscTel
    public String getAfterSuscTel() {
        double iva = iva();
        double afterSuscTelefonia = Double.parseDouble(getBeforeSuscTel())
                + (Double.parseDouble(getBeforeSuscTel()) * iva / (iva > 1 ? 100 : 1));
        return String.format("%.2f", afterSuscTelefonia);
    }

    // tablaDecoAdicional
    public String getTablaDecoAdicional() {

        String result = "";
        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr>" +
                "<td style=\"width: 76px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tecnologia decodificador</strong></span></td>"
                +
                "<td style=\"width: 151px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Valor por renta mensual</strong></span></td></tr>";

        String tablaFila = "<tr>" +
                "<td style=\"width: 76px; height: 24px; text-align: center;\">-hd-</td>" +
                "<td style=\"width: 151px; height: 24px; text-align: center;\">-valor-</td></tr>";

        String tablaFin = "</tbody></table>";

        if (!datosRequest.getSdTvRentaEquiposAd().isEmpty() && !datosRequest.getSdPrecioRentaEquipo().isEmpty()) {

            result += tablaFila.replace("-hd-", datosRequest.getSdTvRentaEquiposAd())
                    .replace("-valor-", "" + datosRequest.getSdPrecioRentaEquipo());
        } else {
            result = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 76px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 151px; height: 24px; text-align: center;\">NO APLICA</td></tr>";
        }

        return tablaIni + result + tablaFin;
    }

    // tablaDecoPrincipal
    public String getTablaDecoPrincipal() {
        String tablaDecoPrincipal = "";

        String result = "";
        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr>" +
                "<td style=\"width: 76px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tecnologia decodificador</strong></span></td>"
                +
                "<td style=\"width: 151px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Valor por renta mensual</strong></span></td></tr>";

        String tablaFila = "<tr>" +
                "<td style=\"width: 76px; height: 24px; text-align: center;\">-hd-</td>" +
                "<td style=\"width: 151px; height: 24px; text-align: center;\">-valor-</td></tr>";

        String tablaFin = "</tbody></table>";

        if (!datosRequest.getHdTvRentaEquiposPr().isEmpty() && !datosRequest.getHdPrecioRentaEquipo().isEmpty()) {

            result += tablaFila.replace("-hd-", datosRequest.getHdTvRentaEquiposPr())
                    .replace("-valor-", "" + datosRequest.getHdPrecioRentaEquipo());
        } else {
            result = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 76px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 151px; height: 24px; text-align: center;\">NO APLICA</td></tr>";
        }

        tablaDecoPrincipal = tablaIni + result + tablaFin;

        return tablaDecoPrincipal;
    }

    // perminBenefCliTv

    public String getPerminBenefCliTv() {
        String perminBenefCliTv = "";
        perminBenefCliTv = datosRequest.getPerminBenefCli().replaceAll("IPTV", "Television")
                .replaceAll("OTT", "Television")
                .replaceAll("Internet", "Television")
                .replaceAll("Telefonia", "Television")
                .replaceAll("telefonia", "Television")
                .replaceAll("internet", "Television");
        return perminBenefCliTv;
    }

    // beforeSuscTel
    public String getBeforeSuscTel() {
        return datosRequest.getBeforeSuscTel();
    }

    // PerminTiempoCli
    public String getPerminTiempoCli() {
        String perminTiempoCli = "";
        perminTiempoCli = datosRequest.getPerminTiempoCli().replaceAll("Telefonia", "IPTV")
                .replaceAll("telefonia", "IPTV")
                .replaceAll("OTT", "IPTV")
                .replaceAll("Internet", "IPTV")
                .replaceAll("internet", "IPTV");

        return perminTiempoCli;
    }

    // telefonoCli
    public String getTelefonoCli() {
        String telefonoCli = "";
        telefonoCli = datosRequest.getPerminBenefCli().replaceAll("Telefonia", "IPTV")
                .replaceAll("telefonia", "IPTV")
                .replaceAll("OTT", "IPTV")
                .replaceAll("Internet", "IPTV")
                .replaceAll("internet", "IPTV");

        return telefonoCli;
    }

    // planActualTelefoniaFormateada

    public String getPlanActualTelefoniaFormateada() {
        String planActualTelefoniaFormateada = "0.00";

        if (datosRequest.getPlanActualTelefonia() != null && !datosRequest.getPlanActualTelefonia().isEmpty()) {
            try {
                double planActualTelefonia = Double.parseDouble(datosRequest.getPlanActualTelefonia());
                planActualTelefoniaFormateada = String.format("%.2f", planActualTelefonia);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Error al calcular el plan actual telefonia.");
            }
        }

        return planActualTelefoniaFormateada;
    }

    public double iva() {
        return Double.parseDouble(datosRequest.getIvaInternet()
                .replace("%", "")
                .trim());
    }

    // AfterSuscTelefonia
    public String getPriceAfterTelefonia() {

        Double priceAfterTelefonia = 0.00;
        try {

            double iva = iva();

            priceAfterTelefonia = Double.parseDouble(datosRequest.getPriceBeforeTelefonia()) +
                    (Double.parseDouble(datosRequest.getPriceBeforeTelefonia()) * iva / (iva > 1 ? 100 : 1));

        } catch (Exception e) {
            priceAfterTelefonia = 0.00;
            throw new NoResultException("Error al calcular el priceAfterTelefonia");
        }

        return String.format("%.2f", priceAfterTelefonia);
    }

    // NombreBancarioCondicional;
    public String getNombreBancarioCondicional() {
        nombreBancarioCondicional = datosRequest.getNombreTitularTarjeta() != null
                || datosRequest.getNombreTitularTarjeta() == "" ? datosRequest.getNombreTitularCta()
                        : datosRequest.getNombreTitularTarjeta();
        return nombreBancarioCondicional;
    }

    // tipoTarjetatipoBanco
    public String getTipoBancoTipoTarjeta() {
        String tipoBancoTipoTarjeta = "";

        String radioButtonPagoFijo = "";
        String[] pagoFijo = { "T", "C", "A" };

        for (int i = 0; i < pagoFijo.length; i++) {
            radioButtonPagoFijo = radioButtonPagoFijo +
                    MetadatosPDA.radioButton(datosRequest.getPagoFormaFijo(), pagoFijo[i],
                            datosRequest.formaPago(pagoFijo[i]));
        }
        if (datosRequest.getPagoFormaFijo().equals("C") || datosRequest.getPagoFormaFijo().equals("A")) {
            tipoBancoTipoTarjeta = radioButtonPagoFijo;
        }
        return tipoBancoTipoTarjeta;
    }

    // tipoTarjetatipoBanco
    public String getTipoTarjetatipoBanco() {
        String tipoTarjetatipoBanco = "";

        String radioButtonPagoFijo = "";
        String[] pagoFijo = { "T", "C", "A" };
        for (int i = 0; i < pagoFijo.length; i++) {
            radioButtonPagoFijo = radioButtonPagoFijo +
                    MetadatosPDA.radioButton(datosRequest.getPagoFormaFijo(), pagoFijo[i],
                            datosRequest.formaPago(pagoFijo[i]));
        }
        if (datosRequest.getPagoFormaFijo().equals("T")) {
            tipoTarjetatipoBanco = radioButtonPagoFijo;
        } else {

        }
        return tipoTarjetatipoBanco;
    }

    // PriceAfterFmc
    public String getPriceAfterFmc() {
        String priceAfterFmc = "";
        if (datosRequest.getPriceBeforeFmc() > 0) {
            Double ivaIncl = (Double.parseDouble(getPriceBeforeFmc()) * 0.12) + Double.parseDouble(getPriceBeforeFmc());
            priceAfterFmc = String.format("%.2f", ivaIncl);
        } else {
            priceAfterFmc = "0.00";
        }
        return priceAfterFmc;
    }

    // PriceBeforeFmc
    public String getPriceBeforeFmc() {
        if (datosRequest.getPriceBeforeFmc() > 0) {
            return String.format("%.2f", datosRequest.getPriceBeforeFmc());
        } else {
            return priceBeforeFmc = "0.00";
        }
    }

    // strDet
    public String getStrDet() {
        List<ServicioFmc> listaServicio = new ArrayList<ServicioFmc>();
        String strDet = "";
        for (int i = 0; i < datosRequest.getDetalleFmc().length; i++) {
            listaServicio.add(datosRequest.getDetalleFmc()[i]);
        }

        if (datosRequest.getTamanioFmc().equals("1")) {
            strDet = listaServicio.get(0).getValor().replaceAll("\\$", "");
        }
        if (datosRequest.getTamanioFmc().equals("2")) {
            strDet = "det0</td><td align='center'>det1";
            strDet = strDet.replaceAll("det0", listaServicio.get(0).getValor().replaceAll("\\$", ""))
                    .replaceAll("det1", listaServicio.get(1).getValor().replaceAll("\\$", ""));
        }
        if (datosRequest.getTamanioFmc().equals("3")) {
            strDet = "det0</td><td align='center'>det1</td><td align='center'>det2";
            strDet = strDet.replaceAll("det0",
                    listaServicio.get(0).getValor().replaceAll("\\$", ""))
                    .replaceAll("det1", listaServicio.get(1).getValor().replaceAll("\\$", ""))
                    .replaceAll("det2", listaServicio.get(2).getValor().replaceAll("\\$", ""));
        }
        return strDet;
    }

    // strCab
    public String getStrCab() {
        List<ServicioFmc> listaServicio = new ArrayList<ServicioFmc>();

        for (int i = 0; i < datosRequest.getDetalleFmc().length; i++) {
            listaServicio.add(datosRequest.getDetalleFmc()[i]);
        }

        if (datosRequest.getTamanioFmc().equals("1")) {
            strCab = listaServicio.get(0).getServicio();
        }
        if (datosRequest.getTamanioFmc().equals("2")) {
            strCab = "cab0</b></td><td align='center' style='background: rgb(255, 1, 12); color: rgb(255, 255, 255);'><b>cab1";
            strCab = strCab.replaceAll("cab0", listaServicio.get(0).getServicio())
                    .replaceAll("cab1", listaServicio.get(1).getServicio());

        }
        if (datosRequest.getTamanioFmc().equals("3")) {
            strCab = "cab0</b></td><td align='center' style='background: rgb(255, 1, 12); color: rgb(255, 255, 255);'><b>cab1</b></td><td align='center' style='background: rgb(255, 1, 12); color: rgb(255, 255, 255);'><b>cab2";
            strCab = strCab.replaceAll("cab0", listaServicio.get(0).getServicio())
                    .replaceAll("cab1", listaServicio.get(1).getServicio())
                    .replaceAll("cab2", listaServicio.get(2).getServicio());

        }
        return strCab;
    }

    // TotalInstalacion
    public String getTotalInstalacion() {
        double totalInstalacion = 0.00;
        try {
            totalInstalacion = Double.parseDouble(getAfterSuscInternet()) + Double.parseDouble(getAfterSuscTelefonia())
                    + Double.parseDouble(getAfterSuscTv());
        } catch (Exception e) {
            totalInstalacion = 0.00;
            throw new NoResultException("Error al calcular el totalInstalacion");
        }
        return String.format("%.2f", totalInstalacion);
    }

    // TotalInstalacionOtt
    public String getTotalInstalacionOtt() {
        double totalInstalacionOtt = 0.00;
        try {
            totalInstalacionOtt = Double.parseDouble(getAfterSuscInternet())
                    + Double.parseDouble(getAfterSuscTelefonia())
                    + Double.parseDouble(getAfterSuscOtt());
        } catch (Exception e) {
            totalInstalacionOtt = 0.00;
            throw new NoResultException("Error al calcular el totalInstalacionOtt");
        }
        return String.format("%.2f", totalInstalacionOtt);
    }

    // AfterSuscTv
    public String getAfterSuscTv() {
        Double afterSuscTelevision = 0.00;
        try {
            if (datosRequest.getTvFijo() == 1) {
                double iva = iva();
                double beforeSusTelevision = Double.parseDouble(datosRequest.getBeforeSuscTv());
                afterSuscTelevision = beforeSusTelevision + (beforeSusTelevision * iva / (iva > 1 ? 100 : 1));
            } else if (datosRequest.getIpTv() == 1) {
                afterSuscTelevision = Double.parseDouble(datosRequest.getPagoInstalacionIptv().getAfterPrice());
            } else {
                afterSuscTelevision = 0.00;
            }

        } catch (Exception e) {
            throw new NoResultException("Error al calcular el afterSuscTelevision," +
                    " por favor revise los campos 'beforeSuscTv', 'afterPrice' en pagoInstalacion, e 'ivaInternet'");
        }
        return String.format("%.2f", afterSuscTelevision);
    }

    // BeforeSuscTv
    public String getBeforeSuscTv() {
        String beforeSuscTv = "";

        if (datosRequest.getTvFijo() == 1) {
            beforeSuscTv = datosRequest.getBeforeSuscTv();
        } else if (datosRequest.getIpTv() == 1) {
            beforeSuscTv = datosRequest.getPagoInstalacionIptv().getBeforePrice();
        } else {
            beforeSuscTv = "0.00";
        }

        return beforeSuscTv;
    }

    // AfterSuscInternet
    public String getAfterSuscInternet() {
        String afterSuscInternet = "";
        try {
            double afterSusInternet = 0.00;
            double iva = (datosRequest.getIvaInternet() == "" || datosRequest.getIvaInternet() == null) ? 0.0
                    : Double.parseDouble(datosRequest.getIvaInternet());
            double beforeSusInternet = (datosRequest.getBeforeSuscInt() == ""
                    || datosRequest.getBeforeSuscInt() == null) ? 0.0
                            : Double.parseDouble(datosRequest.getBeforeSuscInt());
            afterSusInternet = beforeSusInternet + (beforeSusInternet * iva / (iva > 1 ? 100 : 1));
            if (datosRequest.getInternetFijo() == 1) {
                afterSuscInternet = String.format("%.2f", afterSusInternet);
            } else {
                afterSuscInternet = "0.00";
            }
        } catch (Exception e) {
            afterSuscInternet = "0.00";
            throw new NoResultException("Error al calcular parametro afterSuscInternet");
        }
        return afterSuscInternet;
    }

    //// BeforeSuscInternet
    public String getBeforeSuscInternet() {
        String beforeSuscInternet = "";
        if (datosRequest.getInternetFijo() == 1) {
            beforeSuscInternet = datosRequest.getBeforeSuscInt();
        } else {
            beforeSuscInternet = "0.00";
        }
        return beforeSuscInternet;
    }

    // AfterSuscTelefonia
    public String getAfterSuscTelefonia() {
        Double afterSuscTelefonia = 0.00;
        try {
            if (datosRequest.getTelefoniaFijo() == 1) {
                double iva = iva();
                double beforeSusTelefonia = Double.parseDouble(datosRequest.getBeforeSuscTel());
                afterSuscTelefonia = beforeSusTelefonia + (beforeSusTelefonia * iva / (iva > 1 ? 100 : 1));
            } else {
                afterSuscTelefonia = 0.00;
            }
        } catch (Exception e) {
            throw new NoResultException("Error al calcular el afterSuscTelefonia," +
                    " por favor revise los campos 'beforeSuscTel' e 'ivaInternet'");
        }
        return String.format("%.2f", afterSuscTelefonia);
    }

    // BeforeSuscTelefonia
    public String getBeforeSuscTelefonia() {
        double beforeSusctel = 0.00;
        try {

            if (datosRequest.getTelefoniaFijo() == 1) {
                beforeSusctel = Double.parseDouble(datosRequest.getBeforeSuscTel());
            } else {
                beforeSusctel = 0.00;
            }
        } catch (Exception e) {
            beforeSusctel = 0.00;
            throw new NoResultException("Error en los datos 'beforeSusctel'");
        }
        return String.format("%.2f", beforeSusctel);
    }

    // TieneTvFijo
    public String getTieneTvFijo() {
        tieneTvFijo = datosRequest.getTvFijo() == 1 ? "X" : "";
        return tieneTvFijo;
    }

    // TieneInternetFijo
    public String getTieneInternetFijo() {
        tieneInternetFijo = datosRequest.getInternetFijo() == 1 ? "X" : "";
        return tieneInternetFijo;
    }

    // TieneOttFijo
    public String getTieneOttFijo() {
        tieneOttFijo = datosRequest.getOtt() == 1 ? "X" : "";
        return tieneOttFijo;
    }

    // TieneTelefoniaFijo
    public String getTieneTelefoniaFijo() {
        tieneTelefoniaFijo = datosRequest.getTelefoniaFijo() == 1 ? "X" : "";
        return tieneTelefoniaFijo;
    }

    // TablaServiciosPagoUnicoTel
    public String getTablaServiciosPagoUnicoTel() {
        return creaTablaServiciosPagoUnicoTel(datosRequest.getBeforeSuscTv(),
                datosRequest.getAfterSuscTv(), datosRequest.getAdicionalTelevision());
    }

    private String creaTablaServiciosPagoUnicoTel(String beforeSuscTv, String afterSuscTv,
            RQAdicionalTelevision[] adicionales) {

        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr> <td style=\"width: 196px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Detalle</strong></span></td>"
                +
                "<td style=\"width: 76px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Cantidad</strong></span></td>"
                +
                "<td style=\"width: 151px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tarifa mensual sin impuestos</strong></span></td>"
                +
                "<td style=\"width: 142px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Total mensual con impuestos</strong></span></td></tr>";

        String tablaFila = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"><b>-plan-</b></td>" +
                "<td style=\"width: 76px; height: 24px; text-align: center;\">1</td>" +
                "<td style=\"width: 151px; height: 24px; text-align: center;\">-pbin-</td>" +
                "<td style=\"width: 142px; height: 24px; text-align: center;\">-pain-</td></tr>";

        String tablaFin = "</tbody></table>";

        String result = "";

        if (beforeSuscTv != null || afterSuscTv != null) {
            double beforePrice = 0;
            double afterPrice = 0;
            try {
                Double iva = iva();
                beforePrice = Double.parseDouble(beforeSuscTv);
                afterPrice = (beforePrice * iva) + beforePrice;
            } catch (Exception e) {
                throw new NoResultException("Error al calcular el afterPrice y beforePrice");
            }

            result += tablaFila.replace("-plan-", "Precio de Instalaci&oacute;n")
                    .replace("-pbin-", "" + String.format("%.2f", beforePrice))
                    .replace("-pain-", "" + String.format("%.2f", afterPrice));

            if (adicionales != null && adicionales.length > 0) {

                for (int i = 0; i < adicionales.length; i++) {
                    if (adicionales[i].getRecurringFlag() == 0) {
                        result += tablaFila.replace("-plan-", adicionales[i].getPlan())
                                .replace("-pbin-", adicionales[i].getBeforePrice())
                                .replace("-pain-", adicionales[i].getAfterPrice());
                    }
                }
            }

        } else {
            result = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 76px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 151px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 142px; height: 24px; text-align: center;\"></td></tr>";
        }

        return tablaIni + result + tablaFin;
    }

    // AdicionalTelevisor
    public String getAdicionalTelevisor() {
        String radicionalTelevisor = "0.00";
        double adicional = 0;
        try {
            double iva = (datosRequest.getIvaInternet() == "" || datosRequest.getIvaInternet() == null) ? 0.0
                    : Double.parseDouble(datosRequest.getIvaInternet());
            double subtotal = (datosRequest.getPriceBeforeTv() == "" || datosRequest.getPriceBeforeTv() == null) ? 0.0
                    : Double.valueOf(datosRequest.getPriceBeforeTv()) + adicional;

            iva = subtotal * iva / (iva > 1 ? 100 : 1);

            if (datosRequest.getAdicionalTelevision() != null && datosRequest.getAdicionalTelevision().length > 0) {
                for (int i = 0; i < datosRequest.getAdicionalTelevision().length; i++) {
                    if (datosRequest.getAdicionalTelevision()[i].getRecurringFlag() == 1) {
                        adicional += Double.parseDouble((datosRequest.getAdicionalTelevision()[i].getBeforePrice() == ""
                                || datosRequest.getAdicionalTelevision()[i].getBeforePrice() == null) ? "00.00"
                                        : datosRequest.getAdicionalTelevision()[i].getBeforePrice());
                    } else {
                        adicional += Double.parseDouble("00.00");
                    }

                }
            }

            double valorIvaAdicional = adicional + (adicional * iva / (iva > 1 ? 100 : 1));

            radicionalTelevisor = creaTablaServiciosAdicionalesTelevision(datosRequest.getAdicionalTelevision(),
                    adicional,
                    valorIvaAdicional);

        } catch (Exception e) {
            System.out.println("Error al calcular el iva");
            throw new NoResultException("Error al calcular el adicionalTelevisor");
        }
        return radicionalTelevisor;
    }

    private String creaTablaServiciosAdicionalesTelevision(RQAdicionalTelevision[] servicios, double subtotal,
            double iva) {

        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr> <td style=\"width: 196px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Nombre del Plan</strong></span></td>"
                +
                "<td style=\"width: 76px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Cantidad</strong></span></td>"
                +
                "<td style=\"width: 151px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tarifa mensual sin impuestos</strong></span></td>"
                +
                "<td style=\"width: 142px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Total mensual con impuestos</strong></span></td></tr>";

        String tablaFila = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"><b>-plan-</b></td>" +
                "<td style=\"width: 76px; height: 24px; text-align: center;\">1</td>" +
                "<td style=\"width: 151px; height: 24px; text-align: center;\">-pbin-</td>" +
                "<td style=\"width: 142px; height: 24px; text-align: center;\">-pain-</td></tr>";

        String tablaFin = "</tbody></table>";

        StringBuilder result = new StringBuilder();

        if (servicios != null && servicios.length > 0) {
            for (int i = 0; i < servicios.length; i++) {
                double beforePrice = 0.0;
                double afterPrice = 0.0;

                if (servicios[i] != null) {
                    // Verificar si las cadenas son numéricas antes de convertirlas
                    if (isNumeric(servicios[i].getBeforePrice())) {
                        beforePrice = Double.parseDouble(servicios[i].getBeforePrice());
                    }
                    if (isNumeric(servicios[i].getAfterPrice())) {
                        afterPrice = Double.parseDouble(servicios[i].getAfterPrice());
                    }

                    result.append(tablaFila.replace("-plan-", servicios[i].getPlan())
                            .replace("-pbin-", String.format("%.2f", beforePrice))
                            .replace("-pain-", String.format("%.2f", afterPrice)));
                }
            }
        } else {
            result.append("<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 76px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 151px; height: 24px; text-align: center;\"></td>" +
                    "<td style=\"width: 142px; height: 24px; text-align: center;\"></td></tr>");
        }

        return tablaIni + result.toString() + tablaFin;
    }

    // Método auxiliar para verificar si una cadena es numérica
    private boolean isNumeric(String str) {
        boolean resultado = false;
        if (str == null || str == "") {
            return resultado;
        }
        try {
            Double.parseDouble(str);
            resultado = true;
        } catch (NumberFormatException e) {
            resultado = false;
            throw new NoResultException("Error al momento de realizar el parseo de " + str);
        }
        return resultado;
    }

    // RadioButtonTerceraEdad
    public String getRadioButtonTerceraEdad() {
        String[] permite = { "S", "N" };
        String radioButtonTerceraEdad = "";
        for (int i = 0; i < permite.length; i++) {
            radioButtonTerceraEdad = radioButtonTerceraEdad +
                    MetadatosPDA.radioButton(datosRequest.getTerceraEdad(), permite[i], datosRequest.siNo(permite[i]));
        }
        return radioButtonTerceraEdad;
    }

    // RadioButtonClienteDiscap
    public String getRadioButtonClienteDiscap() {
        String[] permite = { "S", "N" };
        String radioButtonClienteDiscap = "";
        for (int i = 0; i < permite.length; i++) {
            radioButtonClienteDiscap = radioButtonClienteDiscap +
                    MetadatosPDA.radioButton(datosRequest.getClienteDiscap(), permite[i],
                            datosRequest.siNo(permite[i]));
        }
        return radioButtonClienteDiscap;
    }

    // TablaPagareResource
    public String getTablaPagareResource() {
        tablaPagareResource = creaTablaPagareResource(datosRequest.getTablaPagareResource());
        return tablaPagareResource;
    }

    private String creaTablaPagareResource(RQTablaPagareResource[] pagare) {
        String tablaIni = "<table style=\"width: 100%; margin: auto;\" border=\"1\" cellspacing=\"0\"><tr>" +
                "<td style=\"width: 10%;\" align=\"center\">NO. CUOTAS</td>" +
                "<td style=\"width: 10%;\" align=\"center\">A DIA FIJO</td>" +
                "<td style=\"width: 25%;\" align=\"center\">VALOR</td>" +
                "<td style=\"width: 55%;\" align=\"center\">VALOR EN LETRAS</td>" + "</tr>";
        String tablaFila = "<tr>" + "<td align=\"center\">-ncout-</td>" + "<td align=\"center\">-diaf-</td>" +
                "<td align=\"center\">-valor-</td>" + "<td align=\"center\">-vlet-</td>" + "</tr>";
        String tablaFin = "</table>";

        StringBuilder result = new StringBuilder();

        if (pagare != null && pagare.length > 0) {
            for (int i = 0; i < pagare.length; i++) {
                if (pagare[i] != null) {
                    result.append(tablaFila.replace("-ncout-", pagare[i].getCuota() != null ? pagare[i].getCuota() : "")
                            .replace("-diaf-", pagare[i].getFechaPago() != null ? pagare[i].getFechaPago() : "")
                            .replace("-valor-", pagare[i].getUsvCouta() != null ? pagare[i].getUsvCouta() : "")
                            .replace("-vlet-", pagare[i].getCuotaLetra() != null ? pagare[i].getCuotaLetra() : ""));
                }
            }
        }
        return tablaIni + result.toString() + tablaFin;
    }

    // FORMATEAR SUMA1 DOS DECIMALES.
    public String getSumaValores1Formateada() {
        return String.format("%.2f", datosRequest.getSumaValores1());
    }

    // FECHA FORMATEADA
    public String getFechaFormateada() {
        try {
            fechaFormateada = String.format("%02d/%02d/%04d", datosRequest.getTramiteDia(),
                    datosRequest.getTramiteMes(), datosRequest.getTramiteAnio());
        } catch (Exception e) {
            fechaFormateada = "% Error en la fecha ingresada %";
            throw new IllegalArgumentException("Error en la fecha Ingresada");
        }
        return fechaFormateada;
    }

    // MES
    public String getMesString() {
        try {
            int numeroMes = this.datosRequest.getTramiteMes();
            if (numeroMes >= 1 && numeroMes <= 12) {
                mesString = MESES_ESPANOL[numeroMes - 1];
            } else {
                mesString = "Mes inválido";
            }
        } catch (NumberFormatException e) {
            mesString = "No es un número válido";
            throw new IllegalArgumentException("No es un número valido");
        }
        return mesString;
    }

    private static final String[] MESES_ESPANOL = {
            "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    };

    // TIPO TARJETA
    public String getTipoTarjeta() {
        String tipoTarjeta = "";
        String[] pagoFijo = { "T", "C", "A" };
        String radioButtonPagoFijo = "";
        for (int i = 0; i < pagoFijo.length; i++) {
            radioButtonPagoFijo = radioButtonPagoFijo +
                    MetadatosPDA.radioButton(datosRequest.getPagoFormaFijo(), pagoFijo[i],
                            formaPago(pagoFijo[i]));
        }
        if (datosRequest.getPagoFormaFijo().equals("T")) {
            tipoTarjeta = radioButtonPagoFijo;
        }
        if (datosRequest.getPagoFormaFijo().equals("C") ||
                datosRequest.getPagoFormaFijo().equals("A")) {
            tipoTarjeta = radioButtonPagoFijo;
        }
        return tipoTarjeta;
    }

    // After Before Internet
    public String getAfterBeforeInternet() {
        String priceAfterInternet = "0.00";
        try {
            String ivaInternet = datosRequest.getIvaInternet();
            String priceBeforeInternet = datosRequest.getPriceBeforeInternet();

            double iva = (ivaInternet != null && !ivaInternet.trim().isEmpty())
                    ? iva()
                    : 0.00;
            double beforeInternet = (priceBeforeInternet != null && !priceBeforeInternet.trim().isEmpty())
                    ? Double.parseDouble(priceBeforeInternet)
                    : 0.00;

            double afterBeforeInternet = beforeInternet + (beforeInternet * iva / (iva > 1 ? 100 : 1));

            priceAfterInternet = String.format("%.2f", afterBeforeInternet);

        } catch (Exception e) {
            priceAfterInternet = "0.00";
            throw new NoResultException("Error al calcular el priceAfterInternet");
        }
        return priceAfterInternet;
    }

    // Tabla de servicio adicionales de internet
    private String creaTablaServiciosAdicionalesInternet(RQAdicionalInternet[] servicios, double subtotal, double iva) {

        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr> <td style=\"width: 196px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Nombre del Plan</strong></span></td>"
                +
                "<td style=\"width: 76px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Cantidad</strong></span></td>"
                +
                "<td style=\"width: 151px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tarifa mensual sin impuestos</strong></span></td>"
                +
                "<td style=\"width: 142px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Total mensual con impuestos</strong></span></td></tr>";

        String tablaFila = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"><b>-plan-</b></td>" +
                "<td style=\"width: 76px; height: 24px; text-align: center;\">1</td>" +
                "<td style=\"width: 151px; height: 24px; text-align: center;\">-pbin-</td>" +
                "<td style=\"width: 142px; height: 24px; text-align: center;\">-pain-</td></tr>";

        String tablaFin = "</tbody></table>";

        String result = "";

        if (servicios != null && servicios.length > 0) {
            for (int i = 0; i < servicios.length; i++) {
                String plan = servicios[i].getPlan();
                String beforePrice = servicios[i].getBeforePrice();
                String afterPrice = servicios[i].getAfterPrice();

                beforePrice = (beforePrice != null && !beforePrice.trim().isEmpty())
                        ? String.format("%.2f", Double.parseDouble(beforePrice))
                        : "0.00";
                afterPrice = (afterPrice != null && !afterPrice.trim().isEmpty())
                        ? String.format("%.2f", Double.parseDouble(afterPrice))
                        : "0.00";

                result += tablaFila.replace("-plan-", plan)
                        .replace("-pbin-", beforePrice)
                        .replace("-pain-", afterPrice);
            }
        } else {
            result = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 76px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 151px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 142px; height: 24px; text-align: center;\">NO APLICA</td></tr>";
        }

        return tablaIni + result + tablaFin;
    }

    // AdicionalInternet
    public String getAdicionalInternet() {
        try {
            double adicional = calcularAdicionalInternet();
            double iva = calcularIVAInternet(adicional);
            double valorIvaAdicional = adicional + (adicional * iva / (iva > 1 ? 100 : 1));
            adicionalInternet = creaTablaServiciosAdicionalesInternet(datosRequest.getAdicionalInternet(), adicional,
                    valorIvaAdicional);
        } catch (Exception e) {
            adicionalInternet = "0.00";
            throw new NoResultException("Error al obtener el adicional de Internet");
        }
        return adicionalInternet;
    }

    // TotalInternet
    public String getTotalInternet() {
        double total = 0;
        try {
            double iva = iva();
            double adicional = calcularAdicionalInternet();
            double subtotal = Double.parseDouble(datosRequest.getPriceBeforeInternet()) + adicional;
            iva = subtotal * iva / (iva > 1 ? 100 : 1);

            total = subtotal + iva;

        } catch (Exception e) {
            total = 0.00;
            throw new NoResultException("Error al calcular el total de Internet");
        }
        return String.format("%.2f", total);
    }

    private double calcularAdicionalInternet() {
        double adicional = 0;
        try {
            RQAdicionalInternet[] adicionalInternet = datosRequest.getAdicionalInternet();
            if (adicionalInternet != null && adicionalInternet.length > 0) {
                for (int i = 0; i < adicionalInternet.length; i++) {
                    adicional += Double.parseDouble(datosRequest.getAdicionalInternet()[i].getBeforePrice());
                }
            }
        } catch (Exception e) {
            adicional = 0;
            throw new NoResultException("Error al calcular el adicional de internet");
        }
        return adicional;
    }

    private double calcularIVAInternet(double monto) {
        double iva = 0.00;
        try {
            String ivaInternet = datosRequest.getIvaInternet();
            iva = (ivaInternet != null && !ivaInternet.trim().isEmpty())
                    ? iva()
                    : 0.00;
            iva = monto * iva / 100;
        } catch (Exception e) {
            iva = 0.00;
            throw new NoResultException("Error al calcular el iva de internet");
        }
        return iva;
    }

    public String getPerminBenefCli() {
        return datosRequest.getPerminBenefCli().replaceAll("IPTV", "Internet")
                .replaceAll("OTT", "Internet")
                .replaceAll("Telefonia", "Internet")
                .replaceAll("Television", "Internet")
                .replaceAll("television", "Internet")
                .replaceAll("telefonia", "Internet");
    }

    // RadioButtonDepositoGarantia
    public String getRadioButtonDepositoGarantia() {
        String[] permite = { "S", "N" };
        String radioButtonDepositoGarantia = "";
        for (int i = 0; i < permite.length; i++) {
            radioButtonDepositoGarantia = radioButtonDepositoGarantia +
                    MetadatosPDA.radioButton(datosRequest.getDepositoGarantiaInt(), permite[i],
                            datosRequest.siNo(permite[i]));
        }
        return radioButtonDepositoGarantia;
    }

    // RadioButtonPerminCli
    public String getRadioButtonPerminCli() {
        String[] permite = { "S", "N" };
        String radioButtonPerminCli = "";
        for (int i = 0; i < permite.length; i++) {
            radioButtonPerminCli = radioButtonPerminCli +
                    MetadatosPDA.radioButton(datosRequest.getPerminCli(), permite[i], datosRequest.siNo(permite[i]));
        }
        return radioButtonPerminCli;
    }

    // RadioButtonPagoFijo
    public String getRadioButtonPagoFijo() {
        String[] pagoFijo = { "T", "C", "A", "F" };
        String radioButtonPagoFijo = "";
        for (int i = 0; i < pagoFijo.length; i++) {
            radioButtonPagoFijo = radioButtonPagoFijo +
                    MetadatosPDA.radioButton(datosRequest.getPagoFormaFijo(), pagoFijo[i], formaPago(pagoFijo[i]));
        }
        return radioButtonPagoFijo;
    }

    // TablaDetalleFinanciamiento
    public String getTablaDetalleFinanciamiento() {
        tablaDetalleFinanciamiento = creaTablaDetalleFinanciamiento(datosRequest.getTablaFinanciamientoSGA());
        return tablaDetalleFinanciamiento;
    }

    // Tabla detalle de financiamiento
    public String creaTablaDetalleFinanciamiento(RQDetallesFinanciamiento[] detallesFinan) {

        String tablaIni = "<table style=\"width: 100%;margin: 0;\" border=\"1\" cellspacing=\"0\"><tr>" +
                "<td style=\"width: 20%;\" align=\"center\">NUMERO FACTURA</td>" +
                "<td style=\"width: 20%;\" align=\"center\">SRI FACT</td>" +
                "<td style=\"width: 15%;\" align=\"center\">FECHA FACT</td>" +
                "<td style=\"width: 15%;\" align=\"center\">USUARIO FACT</td>" +
                "<td style=\"width: 15%;\" align=\"center\">MARCA/MODELO </td>" +
                "<td style=\"width: 15%;\" align=\"center\">VALOR</td>" + "</tr>";
        String tablaFila = "<tr>" + "<td align=\"center\">-nfac-</td>" + "<td align=\"center\">-srif-</td>" +
                "<td align=\"center\">-ffac-</td>" + "<td align=\"center\">-usfac-</td>" +
                "<td align=\"center\">-marmod-</td>" + "<td align=\"center\">-valor-</td>" + "</tr>";
        String tablaFin = "</table>";

        String result = "";

        if (detallesFinan != null && detallesFinan.length > 0) {

            for (int i = 0; i < detallesFinan.length; i++) {

                result += tablaFila.replace("-nfac-", detallesFinan[i].getNumFactura())
                        .replace("-srif-", detallesFinan[i].getSriFact())
                        .replace("-ffac-", detallesFinan[i].getFechaFact())
                        .replace("-usfac-", detallesFinan[i].getUsuarioFact())
                        .replace("-marmod-", detallesFinan[i].getMarcaEquipo())
                        .replace("-valor-", detallesFinan[i].getValor());
            }

        }

        return tablaIni + result + tablaFin;
    }

    // TablaCoutaFinanciamiento
    public String getTablaCoutaFinanciamiento() {
        tablaCoutaFinanciamiento = creaTablaCoutaFinanciamiento(datosRequest.getTablaCuotasFinanciamiento());
        return tablaCoutaFinanciamiento;
    }

    // Tabla de couta de financiamiento
    public String creaTablaCoutaFinanciamiento(RQTablaCuotaFinanciamiento[] coutaFinan) {

        String tablaIni = "<table style=\"width: 100%;margin: auto;\" border=\"1\" cellspacing=\"0\"><tr>" +
                "<td style=\"width: 50%;\" align=\"center\">CONCEPTO</td>" +
                "<td style=\"width: 25%;\" align=\"center\">CUOTAS</td>" +
                "<td style=\"width: 25%;\" align=\"center\">FECHAS DE PAGO</td>" + "</tr>";

        String tablaFila = "<tr>" + "<td align=\"center\">-conc-</td>" + "<td align=\"center\">-cout-</td>" +
                "<td align=\"center\">-fpago-</td>" + "</tr>";

        String tablaFin = "</table>";

        String result = "";

        if (coutaFinan.length > 0 && coutaFinan != null) {

            for (int i = 0; i < coutaFinan.length; i++) {

                result += tablaFila.replace("-conc-", coutaFinan[i].getConcepto())
                        .replace("-cout-", coutaFinan[i].getCuota())
                        .replace("-fpago-", coutaFinan[i].getFechaPago());
            }

        }

        return tablaIni + result + tablaFin;
    }

    private String formaPago(String tipoPago) {
        String result = "";
        switch (tipoPago) {

            case "T":
                result = "Cargo Tarjeta de Cr&eacute;dito:";
                break;
            case "C":
                result = "D&eacute;bito Cta. Cte:";
                break;
            case "A":
                result = "D&eacute;bito Cta. Ahorros:";
                break;
            case "F":
                result = "Contra factura:";
                break;
            case "":
                result = "Cargo al Estado de Cuenta:";
                break;
            default:
                result = "";
        }
        return result;
    }

    // Tabla servicio adicioles de telefonia
    private String creaTablaServiciosAdicionalesTelefonia(RQAdicionalTelefonia[] servicios, double subtotal) {

        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr> <td style=\"width: 20%; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Nombre del Plan</strong></span></td>"
                +
                "<td style=\"width: 20%; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Cantidad</strong></span></td>"
                +
                "<td style=\"width: 20%; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Numero</strong></span></td>"
                +
                "<td style=\"width: 20%; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tarifa mensual sin impuestos</strong></span></td>"
                +
                "<td style=\"width: 20%; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Total mensual con impuestos</strong></span></td></tr>";

        String tablaFila = "<tr>" + "<td style=\"width: 20%; height: 24px; text-align: center;\"><b>-plan-</b></td>" +
                "<td style=\"width: 20%; height: 24px; text-align: center;\">1</td>" +
                "<td style=\"width: 20%; height: 24px; text-align: center;\">-nume-</td>" +
                "<td style=\"width: 20%; height: 24px; text-align: center;\">-pbte-</td>" +
                "<td style=\"width: 20%; height: 24px; text-align: center;\">-pate-</td></tr>";

        String tablaFin = "</tbody></table>";

        String result = "";

        if (servicios.length > 0 && servicios != null) {
            double afterPrice = 0;
            double beforePrice = 0;
            for (int i = 0; i < servicios.length; i++) {
                // if(servicios[i].getRecurringFlag()==1){
                beforePrice = Double.parseDouble(servicios[i].getBeforePrice());
                afterPrice = Double.parseDouble(servicios[i].getAfterPrice());
                result += tablaFila.replace("-plan-", servicios[i].getPlan())
                        .replace("-nume-", servicios[i].getNumero())
                        .replace("-pbte-", String.format("%.2f", beforePrice))
                        .replace("-pate-", String.format("%.2f", afterPrice));
                // }
            }
        } else {
            result = "<tr>" + "<td style=\"width: 20%; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 20%; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 20%; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 20%; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 20%; height: 24px; text-align: center;\">NO APLICA</td></tr>";
        }

        return (tablaIni + result + tablaFin);
    }

    private String creaTablaServiciosAdicionalesIptv(ServicioAdicionalIptv[] servicios, double subtotal, double iva) {

        String tablaIni = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
                "<tbody><tr> <td style=\"width: 196px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Nombre del Plan</strong></span></td>"
                +
                "<td style=\"width: 76px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Cantidad</strong></span></td>"
                +
                "<td style=\"width: 151px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Tarifa mensual sin impuestos</strong></span></td>"
                +
                "<td style=\"width: 142px; height: 1px; background: rgb(255, 1, 12); color: rgb(255, 255, 255); text-align: center;\"><span style=\"font-size:16px;\"><strong>Total mensual con impuestos</strong></span></td></tr>";

        String tablaFila = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\"><b>-plan-</b></td>" +
                "<td style=\"width: 76px; height: 24px; text-align: center;\">1</td>" +
                "<td style=\"width: 151px; height: 24px; text-align: center;\">-pbin-</td>" +
                "<td style=\"width: 142px; height: 24px; text-align: center;\">-pain-</td></tr>";

        String tablaFin = "</tbody></table>";

        String result = "";

        if (servicios.length > 0 && servicios != null) {
            double afterPrice = 0;
            double beforePrice = 0;
            for (int i = 0; i < servicios.length; i++) {
                if (servicios[i].getRecurringFlag() == 1) {
                    beforePrice = Double.parseDouble(servicios[i].getBeforePrice());
                    afterPrice = Double.parseDouble(servicios[i].getAfterPrice());
                    result += tablaFila.replace("-plan-", servicios[i].getPlan())
                            .replace("-pbin-", "" + String.format("%.2f", beforePrice))
                            .replace("-pain-", "" + String.format("%.2f", afterPrice));
                } else {
                    beforePrice = Double.parseDouble(servicios[i].getBeforePrice());
                    afterPrice = Double.parseDouble(servicios[i].getAfterPrice());
                    result += tablaFila.replace("-plan-", servicios[i].getPlan())
                            .replace("-pbin-", "00.00")
                            .replace("-pain-", "00.00");
                }
            }
        } else {
            result = "<tr>" + "<td style=\"width: 196px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 76px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 151px; height: 24px; text-align: center;\">NO APLICA</td>" +
                    "<td style=\"width: 142px; height: 24px; text-align: center;\">NO APLICA</td></tr>";
        }

        return tablaIni + result + tablaFin;
    }

}
