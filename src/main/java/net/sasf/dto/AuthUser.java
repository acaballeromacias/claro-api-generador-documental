
package net.sasf.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AuthUser implements Serializable {

    private String username;

    private String password;
    
    private String grant_type;

}
    