package net.sasf.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MergedPdfResponse {
    private byte[] mergedPdf;
}
